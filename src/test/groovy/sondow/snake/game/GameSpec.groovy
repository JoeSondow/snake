package sondow.snake.game

import sondow.snake.io.EmojiSet
import spock.lang.Specification
import static sondow.snake.game.Move.DOWN
import static sondow.snake.game.Move.LEFT
import static sondow.snake.game.Move.RIGHT
import static sondow.snake.game.Move.UP

class GameSpec extends Specification {

    def 'should make snake game with prod emojis that look good on twitter and bad in intellij'() {
        setup:
        Game game = new Game(EmojiSet.BLUE, new Random(7)).setScore(98765430)

        expect:
        game.toString() == '' +
                'Score 98765430\n' +
                '\n' +
                '▫️▫️▫️▫️▫️▫️▫️▫️▫️\n' +
                '▫️⬛▫️⬛▫️⬛▫️⬛▫️\n' +
                '▫️▫️▫️▫️▫️▫️▫️▫️▫️\n' +
                '▫️⬛▫️⬛▫️⬛▫️⬛▫️\n' +
                '▫️▫️▫️▫️▫️▫️▫️▫️▫️\n' +
                '▫️⬛▫️⬛▫️⬛▫️⬛▫️\n' +
                '▫️▫️▫️▫️▫️▫️▫️▫️▫️\n' +
                '▫️⬛▫️⬛▫️⬛▫️⬛▫️\n' +
                '▫️▫️▫️▫️▫️▫️▫️▫️▫️\n' +
                '▫️⬛▫️⬛▫️⬛▫️⬛▫️\n' +
                '▫️▫️▫️▫️▫️▫️▫️▫️▫️'
    }

    def 'should create snake game with monospaced test emojis suitable for Windows development'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(7))
        Snake snake = new Snake().addKeyPoint(6, 2).addKeyPoint(2, 2).addKeyPoint(2, 4)
        Target target = new Target(8, 6)
        game.setScore(98765430).setTarget(target).setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 98765430\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕😋⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
    }

    def 'should spawn new snake at random location'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(23))

        when:
        String before = game.toString()
        game.spawnSnake()
        String after1 = game.toString()
        game.spawnSnake()
        String after2 = game.toString()

        then:
        before == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        after1 == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵😋⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        after2 == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕🔵🔵😋⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
    }

    def 'should spawn new target at random location'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(23))
        game.spawnSnake()

        when:
        String before = game.toString()
        game.spawnTarget()
        String after1 = game.toString()
        game.spawnTarget()
        String after2 = game.toString()
        game.spawnTarget()
        String after3 = game.toString()

        then:
        before == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵😋⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        after1 == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵😋⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        after2 == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕🔵🔵😋⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        after3 == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕🔵🔵😛⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
    }

    def "should list legal moves when tail is about to get out of head's way"() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(8, 8).addKeyPoint(6, 8).addKeyPoint(6, 6).
                addKeyPoint(8, 6).setAtomicMovesSinceEating(1)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕🙂\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [LEFT, DOWN]
    }

    def 'should list legal moves when body is in the way'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(8, 8).addKeyPoint(6, 8).addKeyPoint(6, 6).
                addKeyPoint(10, 6).setAtomicMovesSinceEating(2)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕🙂\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕⭕'
        game.listLegalMoves() == [DOWN]
    }

    def 'should list legal moves when nothing is in the way except the grid right edge'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(8, 8).addKeyPoint(10, 8).addKeyPoint(10, 4).
                addKeyPoint(2, 4).setAtomicMovesSinceEating(3)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕🙂\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛🔵\n' +
                '⭕⭕⭕⭕🔵🔵🔵🔵🔵'
        game.listLegalMoves() == [UP, LEFT]
    }

    def 'should list legal moves when nothing is in the way except the grid left edge'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(8, 0).addKeyPoint(10, 0).addKeyPoint(10, 4).
                addKeyPoint(2, 4).setAtomicMovesSinceEating(4)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '🙂⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '🔵⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '🔵🔵🔵🔵🔵⭕⭕⭕⭕'
        game.listLegalMoves() == [UP, RIGHT]
    }

    def 'should list legal moves when nothing is in the way of short snake except grid top edge'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(0, 4).addKeyPoint(0, 2).setAtomicMovesSinceEating(5)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕🔵🔵😐⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [RIGHT, DOWN]
    }

    def 'should list legal moves when nothing is in the way of long snake except grid top edge'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(0, 4).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(6)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕🔵🔵😐⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [RIGHT, DOWN]
    }

    def 'should list legal moves for long straight snake at top edge'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(0, 4).addKeyPoint(0, 0).setAtomicMovesSinceEating(7)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '🔵🔵🔵🔵😐⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [RIGHT, DOWN]
    }

    def 'should list legal moves for where going left would make tail get out of the way'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(0, 4).addKeyPoint(2, 4).addKeyPoint(2, 2).
                addKeyPoint(0, 2).setAtomicMovesSinceEating(8)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕🔵⭕😐⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [LEFT, RIGHT]
    }

    def 'should list legal moves for winding snake at top'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(0, 4).addKeyPoint(2, 4).addKeyPoint(2, 2).
                addKeyPoint(0, 2).addKeyPoint(0, 0).setAtomicMovesSinceEating(9)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '🔵🔵🔵⭕😟⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [RIGHT]
    }

    def 'should list legal moves when nothing is in the way'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(6, 4).addKeyPoint(2, 4).setAtomicMovesSinceEating(10)
        game.setSnake(snake)

        expect:
        game.toString() == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕😟⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        game.listLegalMoves() == [LEFT, RIGHT, DOWN]
    }

    def 'should go in each direction when snake is short and not eating'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        game.spawnSnake()
        game.spawnTarget()

        when:
        String before = game.toString()
        game.doCompositeMove(RIGHT)
        String afterRight = game.toString()
        game.doCompositeMove(DOWN)
        String afterDown = game.toString()
        game.doCompositeMove(LEFT)
        String afterLeft = game.toString()
        game.doCompositeMove(UP)
        String afterUp = game.toString()

        then:
        before == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕🔵⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕😋⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕🕛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔺⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        afterRight == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🙂⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕🕚⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔺⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        afterDown == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🙂⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕🕙⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔺⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        afterLeft == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🙂🔵🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕🕘⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔺⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        afterUp == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🙂⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕🕗⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔺⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
    }

    def 'should move snake around'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(6, 4).addKeyPoint(2, 4).setAtomicMovesSinceEating(7)
        game.setSnake(snake)
        game.spawnTarget()

        when:
        String before = game.toString()
        List<Move> movesBefore = game.listLegalMoves()
        game.doCompositeMove(RIGHT)
        String afterRight = game.toString()
        List<Move> movesAfterRight = game.listLegalMoves()
        game.doCompositeMove(UP)
        String afterUp = game.toString()
        List<Move> movesAfterUp = game.listLegalMoves()
        game.doCompositeMove(UP)
        String after2Up = game.toString()
        List<Move> movesAfter2Up = game.listLegalMoves()
        game.doCompositeMove(UP)
        String after3Up = game.toString()
        List<Move> movesAfter3Up = game.listLegalMoves()
        game.doCompositeMove(RIGHT)
        String after2Right = game.toString()
        List<Move> movesAfter2Right = game.listLegalMoves()

        then:
        before == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕔⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕😐⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesBefore == [LEFT, RIGHT, DOWN]
        afterRight == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕓⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵🔵😐⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfterRight == [UP, RIGHT, DOWN]
        afterUp == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕒⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕😟⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕🔵🔵🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfterUp == [UP, LEFT, RIGHT]
        after2Up == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕑⭕\n' +
                '⭕⭕⭕⭕⭕⭕😛⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfter2Up == [UP, LEFT, RIGHT]
        after3Up == '' +
                'Score 20\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕😋⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕🔺\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfter3Up == [LEFT, RIGHT]
        after2Right == '' +
                'Score 20\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵⭕🙂\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕙⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕🔺\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfter2Right == [LEFT, DOWN]
    }

    def 'should show game over screen'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(2, 2).addKeyPoint(0, 2).
                addKeyPoint(0, 6).addKeyPoint(4, 6).addKeyPoint(4, 0)
        game.setSnake(snake)
        game.spawnTarget()

        when:
        String before = game.toString()
        List<Move> movesBefore = game.listLegalMoves()
        boolean isGameOverBefore = game.isGameOver()
        game.doCompositeMove(RIGHT)
        String after = game.toString()
        List<Move> movesAfter = game.listLegalMoves()
        boolean isGameOverAfter = game.isGameOver()

        then:
        before == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕🔵🔵🔵🔵🔵⭕⭕\n' +
                '⭕⬛🔵⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕😋⭕⭕⭕🔵⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '🔵🔵🔵🔵🔵🔵🔵⭕⭕\n' +
                '⭕🕛⭕⬛⭕⬛⭕⬛⭕\n' +
                '🔺⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesBefore == [LEFT, RIGHT]
        !isGameOverBefore
        after == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕🔴🔴🔴🔴🔴⭕⭕\n' +
                '⭕⬛🔴⬛⭕⬛🔴⬛⭕\n' +
                '⭕⭕🔴🔴😞⭕🔴⭕⭕\n' +
                '⭕🇬⭕🇦⭕🇲🔴🇪⭕\n' +
                '⭕⭕🔴🔴🔴🔴🔴⭕⭕\n' +
                '⭕🇴⭕🇻⭕🇪⭕🇷⭕\n' +
                '💥⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfter == []
        isGameOverAfter
    }

    def 'should score zero points for eating an apple after 12 atomic moves'() {
        setup:
        Game game = new Game(EmojiSet.TEST, new Random(12))
        Snake snake = new Snake().addKeyPoint(2, 4).addKeyPoint(6, 4).setAtomicMovesSinceEating(11)
        game.setSnake(snake)
        game.spawnTarget()

        when:
        String before = game.toString()
        List<Move> movesBefore = game.listLegalMoves()
        game.doCompositeMove(RIGHT)
        String afterRight = game.toString()
        List<Move> movesAfterRight = game.listLegalMoves()
        game.doCompositeMove(UP)
        String afterUp = game.toString()

        then:
        before == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕐⭕\n' +
                '⭕⭕⭕⭕😟⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesBefore == [UP, LEFT, RIGHT]
        afterRight == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🚫⭕\n' +
                '⭕⭕⭕⭕🔵🔵😛⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        movesAfterRight == [UP, RIGHT, DOWN]
        afterUp == '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕😋⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛⭕\n' +
                '⭕⭕⭕⭕🔵🔵🔵⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕🔺\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
    }

    def "eating target should increase score based on how long since eating"() {
        setup:
        Game game = new Game(EmojiSet.BLUE, new Random(6)).setScore(400)

        when:
        int scoreA = game.getScore()

        then:
        scoreA == 400

        when:
        game.scoreEatenTarget(3)

        then:
        game.getScore() == 490

        when:
        game.scoreEatenTarget(6)

        then:
        game.getScore() == 550

        when:
        game.scoreEatenTarget(5)

        then:
        game.getScore() == 620

        when:
        game.scoreEatenTarget(1)

        then:
        game.getScore() == 730

        when:
        game.scoreEatenTarget(11)

        then:
        game.getScore() == 740

        when:
        game.scoreEatenTarget(12)

        then:
        game.getScore() == 740

        when:
        game.scoreEatenTarget(13)

        then:
        game.getScore() == 740

        when:
        game.scoreEatenTarget(14)

        then:
        game.getScore() == 740

        when:
        game.scoreEatenTarget(15)

        then:
        game.getScore() == 740
    }
}
