package sondow.snake.game

import java.time.ZonedDateTime
import spock.lang.Specification
import static sondow.snake.io.EmojiSet.FORTUNE

class GameRecordSpec extends Specification {

    ZonedDateTime d(String day) {
        ZonedDateTime.parse("2019-05-${day}T12:34:56Z")
    }

    def "should sort game records based on score descending then id ascending"() {
        setup:
        GameRecord r1 = new GameRecord(1L, [FORTUNE], 1900, 1234L, d('06'), 1678L, d('07'))
        GameRecord r2 = new GameRecord(2L, [FORTUNE], 2300, 3234L, d('07'), 2678L, d('08'))
        GameRecord r3 = new GameRecord(3L, [FORTUNE], 2100, 4234L, d('09'), 3678L, d('09'))
        GameRecord r4 = new GameRecord(4L, [FORTUNE], 2800, 5234L, d('10'), 4678L, d('11'))
        GameRecord r5 = new GameRecord(5L, [FORTUNE], 2100, 6234L, d('11'), 5678L, d('12'))
        GameRecord r6 = new GameRecord(6L, [FORTUNE], 2400, 7234L, d('12'), 6678L, d('13'))
        List<GameRecord> records = [r2, r6, r5, r1, r3, r4]

        when:
        TreeSet<GameRecord> sorted = new TreeSet<>(records)

        then: 'Higher scores first, ties are won by who got the score first so lower ids first'
        List<GameRecord> sortedList = sorted.toList()
        records == [r2, r6, r5, r1, r3, r4]
        sortedList == [r4, r6, r2, r3, r5, r1]
        sortedList*.id == [4, 6, 2, 3, 5, 1]
        sortedList*.score == [2800, 2400, 2300, 2100, 2100, 1900]
    }
}
