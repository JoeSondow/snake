package sondow.snake.game

import spock.lang.Specification

class SnakeSpec extends Specification {

    def "should throw exception if adding key segments in same location"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(2, 6)

        when:
        boolean thrown = false
        String msg = null
        try {
            snake.addKeyPoint(2, 6)
        } catch (RuntimeException e) {
            thrown = true
            msg = e.message
        }

        then:
        thrown
        msg == "Invalid snake data. Multiple key points at row 2 column 6"
    }

    def "should throw exception if adding key segments that do not share a row or column"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(2, 6)

        when:
        boolean thrown = false
        String msg = null
        try {
            snake.addKeyPoint(4, 8)
        } catch (RuntimeException e) {
            thrown = true
            msg = e.message
        }

        then:
        thrown
        msg == "Invalid snake data. Key Point{row=2, col=6} does not share a row or column " +
                "with next key Point{row=4, col=8}"
    }

    private Point p(int r, int c) {
        new Point(r, c)
    }

    def "should generate complete short snake when head is below tail"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(6, 4)
        snake.addKeyPoint(2, 4)

        when:
        List<Point> allSegments = snake.getAllPoints()

        then:
        allSegments == [p(6, 4), p(5, 4), p(4, 4), p(3, 4), p(2, 4)]
    }

    def "should generate complete short snake when head is above tail"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(2, 4)
        snake.addKeyPoint(6, 4)

        when:
        List<Point> allSegments = snake.getAllPoints()

        then:
        allSegments == [p(2, 4), p(3, 4), p(4, 4), p(5, 4), p(6, 4)]
    }

    def "should generate complete short snake when head is to the right of tail"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 6)
        snake.addKeyPoint(4, 2)

        when:
        List<Point> allSegments = snake.getAllPoints()

        then:
        allSegments == [p(4, 6), p(4, 5), p(4, 4), p(4, 3), p(4, 2)]
    }

    def "should generate complete short snake when head is to the left of tail"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 2)
        snake.addKeyPoint(4, 6)

        when:
        List<Point> allSegments = snake.getAllPoints()

        then:
        allSegments == [p(4, 2), p(4, 3), p(4, 4), p(4, 5), p(4, 6)]
    }

    def "should generate snake from upper left to upper right to lower right"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(0, 2)
        snake.addKeyPoint(0, 4)
        snake.addKeyPoint(4, 4)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(0, 2), p(0, 3), p(0, 4), p(1, 4), p(2, 4), p(3, 4), p(4, 4)]
    }

    def "should generate snake from upper right to lower right to lower left"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(0, 4)
        snake.addKeyPoint(2, 4)
        snake.addKeyPoint(2, 2)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(0, 4), p(1, 4), p(2, 4), p(2, 3), p(2, 2)]
    }

    def "should generate snake from upper right to upper left to lower left"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(0, 4)
        snake.addKeyPoint(0, 0)
        snake.addKeyPoint(4, 0)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(0, 4), p(0, 3), p(0, 2), p(0, 1), p(0, 0),
                      p(1, 0), p(2, 0), p(3, 0), p(4, 0)]
    }

    def "should generate snake from lower right to lower left to upper left"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(6, 4)
        snake.addKeyPoint(6, 2)
        snake.addKeyPoint(4, 2)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(6, 4), p(6, 3), p(6, 2), p(5, 2), p(4, 2)]
    }

    def "should generate snake from lower left to upper left to upper right"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(6, 4)
        snake.addKeyPoint(4, 4)
        snake.addKeyPoint(4, 6)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(6, 4), p(5, 4), p(4, 4), p(4, 5), p(4, 6)]
    }

    def "should generate snake from upper left to lower left to lower right"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(0, 0)
        snake.addKeyPoint(0, 2)
        snake.addKeyPoint(2, 2)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(0, 0), p(0, 1), p(0, 2), p(1, 2), p(2, 2)]
    }

    def "should generate snake from lower left to lower right to upper right"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 2)
        snake.addKeyPoint(4, 4)
        snake.addKeyPoint(2, 4)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(4, 2), p(4, 3), p(4, 4), p(3, 4), p(2, 4)]
    }

    def "should generate snake from lower right to upper right to upper left"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(6, 6)
        snake.addKeyPoint(4, 6)
        snake.addKeyPoint(4, 4)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(6, 6), p(5, 6), p(4, 6), p(4, 5), p(4, 4)]
    }

    def "should generate complex snake from long list of key points"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 4)
        snake.addKeyPoint(0, 4)
        snake.addKeyPoint(0, 2)
        snake.addKeyPoint(2, 2)
        snake.addKeyPoint(2, 0)
        snake.addKeyPoint(6, 0)
        snake.addKeyPoint(6, 6)
        snake.addKeyPoint(2, 6)

        when:
        List<Point> allPoints = snake.getAllPoints()

        then:
        allPoints == [p(4, 4), p(3, 4), p(2, 4), p(1, 4), p(0, 4),
                      p(0, 3), p(0, 2), p(1, 2), p(2, 2), p(2, 1),
                      p(2, 0), p(3, 0), p(4, 0), p(5, 0), p(6, 0),
                      p(6, 1), p(6, 2), p(6, 3), p(6, 4), p(6, 5),
                      p(6, 6), p(5, 6), p(4, 6), p(3, 6), p(2, 6)]
    }

    def "should get the tail end of the snake"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(2, 0)
        snake.addKeyPoint(6, 0)
        snake.addKeyPoint(6, 6)
        snake.addKeyPoint(2, 6)

        when:
        Point tail = snake.getTailEnd()

        then:
        tail == p(2, 6)
    }

    def "should get the tail end of a short snake"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 6)
        snake.addKeyPoint(2, 6)

        when:
        Point tail = snake.getTailEnd()

        then:
        tail == p(2, 6)
    }

    def "should get all points except tail end of a short snake"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 6)
        snake.addKeyPoint(2, 6)

        when:
        List<Point> allPointsExceptTailEnd = snake.getAllPointsExceptTailEnd()

        then:
        allPointsExceptTailEnd == [p(4, 6), p(3, 6)]
    }

    def "should get all points except tail end of a long straight snake"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(6, 6)
        snake.addKeyPoint(2, 6)

        when:
        List<Point> allPointsExceptTailEnd = snake.getAllPointsExceptTailEnd()

        then:
        allPointsExceptTailEnd == [p(6, 6), p(5, 6), p(4, 6), p(3, 6)]
    }

    def "should get all points except tail end of a bendy snake"() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(4, 6)
        snake.addKeyPoint(2, 6)
        snake.addKeyPoint(2, 2)

        when:
        List<Point> allPointsExceptTailEnd = snake.getAllPointsExceptTailEnd()

        then:
        allPointsExceptTailEnd == [p(4, 6), p(3, 6), p(2, 6), p(2, 5), p(2, 4), p(2, 3)]
    }

    def 'should interpolate all intersection points on snake as key points'() {
        setup:
        Snake snake = new Snake()
        snake.addKeyPoint(2, 8)
        snake.addKeyPoint(6, 8)
        snake.addKeyPoint(6, 2)
        snake.addKeyPoint(2, 2)
        snake.addKeyPoint(2, 6)

        when:
        List<Point> keyPointsBeforeInterpolation = snake.getKeyPoints()
        List<Point> keyPointsAfterInterpolation = snake.interpolateIntersectionPoints()

        then:
        keyPointsBeforeInterpolation == [p(2, 8), p(6, 8), p(6, 2), p(2, 2), p(2, 6)]
        keyPointsAfterInterpolation == [p(2, 8), p(4, 8), p(6, 8),
                                        p(6, 6), p(6, 4), p(6, 2), p(4, 2),
                                        p(2, 2), p(2, 4), p(2, 6)]
    }

    def 'should extrapolate minimum key points from all intersection points'() {
        setup:
        Snake snake = new Snake()
        snake.setKeyPoints([p(2, 8), p(4, 8), p(6, 8),
                            p(6, 6), p(6, 4), p(6, 2), p(4, 2),
                            p(2, 2), p(2, 4), p(2, 6)])

        when:
        List<Point> keyPointsBeforeExtrapolation = snake.getKeyPoints()
        snake.extrapolateKeyPoints()
        List<Point> keyPointsAfterExtrapolation = snake.getKeyPoints()

        then:
        keyPointsBeforeExtrapolation == [p(2, 8), p(4, 8), p(6, 8),
                                         p(6, 6), p(6, 4), p(6, 2), p(4, 2),
                                         p(2, 2), p(2, 4), p(2, 6)]
        keyPointsAfterExtrapolation == [p(2, 8), p(6, 8), p(6, 2), p(2, 2), p(2, 6)]
    }

    def 'should make a deep copy with a separate key points list'() {
        setup:
        Snake original = new Snake().addKeyPoint(4, 6).addKeyPoint(4, 8).addKeyPoint(2, 8).
                setAtomicMovesSinceEating(13)
        List<Point> originalKeyPoints = original.getKeyPoints()

        when:
        Snake copy = original.deepCopy()
        List<Point> copyKeyPoints = copy.getKeyPoints()

        then:
        copy == original
        !copy.is(original)
        originalKeyPoints == copyKeyPoints
        !originalKeyPoints.is(copyKeyPoints)

        when:
        originalKeyPoints.add(new Point(2, 2))

        then:
        originalKeyPoints.size() == 4
        copyKeyPoints.size() == 3
        originalKeyPoints != copyKeyPoints
        copy != original
    }
}
