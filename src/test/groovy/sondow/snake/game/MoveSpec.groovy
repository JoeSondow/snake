package sondow.snake.game

import spock.lang.Specification
import static sondow.snake.game.Move.DOWN
import static sondow.snake.game.Move.LEFT
import static sondow.snake.game.Move.RIGHT
import static sondow.snake.game.Move.UP

class MoveSpec extends Specification {

    def 'should lowercase and strip out non-letters'() {

        expect:
        result == Move.lowercaseLettersOnly(input)

        where:
        input      | result
        'down'     | 'down'
        'Down'     | 'down'
        '  DOWN  ' | 'down'
        "⬆️ Up"    | 'up'
        "⬆ Up"     | 'up'
        "⬅️ Left"  | 'left'
        "⬅ Left"   | 'left'
        "➡️ Right" | 'right'
        "➡ Right"  | 'right'
        "⬇️ Down"  | 'down'
        "⬇ Down"   | 'down'
    }

    def 'should choose correct Move from move name with or without emojis'() {
        expect:
        result == Move.fromLabel(label)

        where:
        label      | result
        'down'     | DOWN
        'Down'     | DOWN
        '  DOWN  ' | DOWN
        "⬆️ Up"    | UP
        "⬆ Up"     | UP
        "⬅️ Left"  | LEFT
        "⬅ Left"   | LEFT
        "➡️ Right" | RIGHT
        "➡ Right"  | RIGHT
        "⬇️ Down"  | DOWN
        "⬇ Down"   | DOWN
    }

    def "should get Move enumeration items from database abbreviations"() {
        expect:
        UP == Move.fromAbbreviation("up")
        LEFT == Move.fromAbbreviation("lf")
        RIGHT == Move.fromAbbreviation("ri")
        DOWN == Move.fromAbbreviation("dn")
    }

    def "should get Move enumeration items from poll entry strings"() {
        expect:
        UP == Move.fromEmojiAndTitleCase("⬆️ Up")
        LEFT == Move.fromEmojiAndTitleCase("⬅️ Left")
        RIGHT == Move.fromEmojiAndTitleCase("➡️ Right")
        DOWN == Move.fromEmojiAndTitleCase("⬇️ Down")
    }

    def "should make poll choices from moves and preserve order"() {
        when:
        String up = "⬆️ Up"
        String left = "⬅️ Left"
        String right = "➡️ Right"
        String down = "⬇️ Down"

        then:
        [up] == Move.toPollChoices([UP])
        [left] == Move.toPollChoices([LEFT])
        [right] == Move.toPollChoices([RIGHT])
        [down] == Move.toPollChoices([DOWN])
        [up, left, right, down] == Move.toPollChoices([UP, LEFT, RIGHT, DOWN])
        [left, right, down] == Move.toPollChoices([LEFT, RIGHT, DOWN])
        [up, right, down] == Move.toPollChoices([UP, RIGHT, DOWN])
        [right, down] == Move.toPollChoices([RIGHT, DOWN])
        [up, left] == Move.toPollChoices([UP, LEFT])
    }
}
