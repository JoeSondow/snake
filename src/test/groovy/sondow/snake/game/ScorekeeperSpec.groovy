package sondow.snake.game

import java.time.ZonedDateTime
import spock.lang.Specification
import static sondow.snake.io.EmojiSet.BREAD
import static sondow.snake.io.EmojiSet.FORTUNE
import static sondow.snake.io.EmojiSet.GIFT
import static sondow.snake.io.EmojiSet.PEACH
import static sondow.snake.io.EmojiSet.TREE

class ScorekeeperSpec extends Specification {

    ZonedDateTime d(String day, String hour) {
        ZonedDateTime.parse("2019-05-${day}T${hour}:34:56Z")
    }

    def 'should add new score to empty list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(40)
        GameRecord record = new GameRecord(1, [FORTUNE], 130, 11, d('14', '09'), 19, d('15', '16'))

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate([], record)

        then:
        newRecords == [record]
    }

    def 'should add new high score to top of short list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(40)
        GameRecord newR270 = new GameRecord(5, [BREAD], 270, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 29L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR270)

        then:
        newRecords == [newR270, r260, r240, r130, r70]
    }

    def 'should add new score to bottom of short list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(40)
        GameRecord newR60 = new GameRecord(5, [BREAD], 60, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 229L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR60)

        then:
        newRecords == [r260, r240, r130, r70, newR60]
    }

    def 'should add new score to middle of short list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(40)
        GameRecord newR180 = new GameRecord(5, [BREAD], 180, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 29L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR180)

        then:
        newRecords == [r260, r240, newR180, r130, r70]
    }

    def 'should add new score to top of full list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(6)
        GameRecord newR270 = new GameRecord(7, [BREAD], 270, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r120 = new GameRecord(5, [GIFT], 120, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r110 = new GameRecord(6, [GIFT], 110, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 29L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r120, r110, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR270)

        then:
        newRecords == [newR270, r260, r240, r130, r120, r110]
    }

    def 'should add new score to middle of full list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(6)
        GameRecord newR180 = new GameRecord(7, [BREAD], 180, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r120 = new GameRecord(5, [GIFT], 120, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r110 = new GameRecord(6, [GIFT], 110, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 29L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r120, r110, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR180)

        then:
        newRecords == [r260, r240, newR180, r130, r120, r110]
    }

    def 'should add new score to bottom of full list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(6)
        GameRecord newR80 = new GameRecord(7, [BREAD], 80, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r120 = new GameRecord(5, [GIFT], 120, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r110 = new GameRecord(6, [GIFT], 110, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 29L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r120, r110, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR80)

        then:
        newRecords == [r260, r240, r130, r120, r110, newR80]
    }

    def 'should add new score that is so low it does not get on the new list'() {
        setup:
        Scorekeeper scorekeeper = new Scorekeeper(6)
        GameRecord newR60 = new GameRecord(7, [BREAD], 60, 55L, d('14', '09'), 59L, d('15', '09'))

        GameRecord r260 = new GameRecord(3, [FORTUNE], 260, 33L, d('07', '09'), 39L, d('08', '09'))
        GameRecord r240 = new GameRecord(4, [PEACH], 240, 44L, d('08', '09'), 49L, d('09', '09'))
        GameRecord r130 = new GameRecord(1, [GIFT], 130, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r120 = new GameRecord(5, [GIFT], 120, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r110 = new GameRecord(6, [GIFT], 110, 11L, d('05', '09'), 19L, d('06', '09'))
        GameRecord r70 = new GameRecord(2, [TREE], 70, 22L, d('06', '09'), 29L, d('07', '09'))

        List<GameRecord> oldRecords = [r260, r240, r130, r120, r110, r70]

        when:
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(oldRecords, newR60)

        then:
        newRecords == oldRecords
    }
}
