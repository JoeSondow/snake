package sondow.snake.game

import java.time.ZonedDateTime
import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import sondow.snake.conf.Environment
import sondow.snake.conf.FileClerk
import sondow.snake.conf.Keymaster
import sondow.snake.conf.Time
import sondow.snake.io.Database
import sondow.snake.io.EmojiSet
import sondow.snake.io.Outcome
import sondow.snake.io.TwitterPollMaker
import spock.lang.Specification
import spock.lang.Unroll
import twitter4j.Card
import twitter4j.Choice
import twitter4j.Status
import twitter4j.StatusWithCard

class PlayerSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    private void initEnvironment() {
        String filler = Environment.SPACE_FILLER
        envVars.set("cred_twitter_reading",
                "${filler}disney,mickey,donald,goofy,pluto${filler}")
        envVars.set("cred_twitter_writing",
                "${filler}${filler}EmojiTetraGamma,hannahbarbera,looneytunes,ghostpowder," +
                        "auntfrenchie${filler}")
        envVars.set('cred_airtable_personal_access_token', 'booyah')
        envVars.set('cred_airtable_base', 'yeahboi')
    }

    def "should tweet without a poll when game ends and tweet high scores"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 90),
                new Choice("➡️ Right", 10) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String before = "" +
                "Score 80\n" +
                "\n" +
                "🔵🔵🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛🔵⬛⭕⬛⭕🕛⭕\n" +
                "🔵⭕🔵⭕⭕⭕😛⭕🔺\n" +
                "🔵⬛🔵⬛⭕⬛🔵⬛⭕\n" +
                "🔵⭕🔵🔵🔵🔵🔵⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
        String after = '' +
                'Score 80\n' +
                '\n' +
                '🔴🔴🔴🔴🔴🔴🔴🔴🔴\n' +
                '🔴⬛⭕⬛⭕⬛⭕⬛🔴\n' +
                '🔴⭕🔴🔴🔴🔴🔴🔴🔴\n' +
                '🔴🇬🔴🇦⭕🇲⭕🇪⭕\n' +
                '🔴⭕🔴⭕😞🔴🔴⭕💥\n' +
                '⭕🇴🔴🇻⭕🇪🔴🇷⭕\n' +
                '⭕⭕🔴🔴🔴🔴🔴⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'

        Status tweet = Mock()
        Date createdAt = Date.from(ZonedDateTime.parse('2019-06-20T06:28:34Z').toInstant())

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        outcome.previousGameString == before
        outcome.tweets.size() == 1
        1 * database.readGameState() >>
                '{"thm":"TEST","thr":31,"sn":[4,6,"D2L4U4R6U2L8D6"],' +
                '"tg":[4,8],"sc":80,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        2 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(after, 1234567890, null) >> tweet
        4 * tweet.getId() >> 8L
        1 * database.readPlaythroughAndScores() >>
                new PlaythroughAndScores('1-9-2019062006-b', oldScoresDb)
        1 * tweet.getCreatedAt() >> createdAt
        1 * database.deleteStateAndUpdateScores(newScoresDb)
        outcome.tweets == [tweet]
        1 * pollMaker.tweet(highScoresTweet, 8, null)
        0 * _._

        where:
        oldScoresDb                                       |
                newScoresDb                                                            |
                highScoresTweet
        null                                              |
                '1b80-9-wg3*:-8-wg3*:'                                                 |
                'High Scores:\n\n* 1. 80 #1 🍞 0h *'
        '31b2680-6-wg0h!-8-wg0i3'                         |
                '31b2680-6-wg0h!-8-wg0i3,1b80-9-wg3*:-8-wg3*:'                         |
                'High Scores:\n\n1. 2680 #31 🍞 21h\n* 2. 80 #1 🍞 0h *'
        '31b2680-6-wg0h!-8-wg0i3,90u2650-3-wg3WY-5-wg3X~' |
                '31b2680-6-wg0h!-8-wg0i3,90u2650-3-wg3WY-5-wg3X~,1b80-9-wg3*:-8-wg3*:' |
                'High Scores:\n\n1. 2680 #31 🍞 21h\n2. 2650 #90 🔵 23h\n* 3. 80 #1 🍞 0h *'
    }

    def 'game should end when snake meets tail and every intersection is full of snake'() {

        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Up", 2),
                new Choice("➡️ Right", 90) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previous = "" +
                "Score 210\n" +
                "\n" +
                "🔵🔵🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛🔵⬛⭕⬛⭕⬛⭕\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛🔵⬛⭕⬛⭕🕕⭕\n" +
                "🔵⭕🔵🔵🔵🔵🔵⭕🔺\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "🔵🔵🔵🔵🔵🔵😐⭕⭕"
        String text1 = "" +
                "Score 210\n" +
                "\n" +
                "🔵🔵🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛🔵⬛⭕⬛⭕⬛⭕\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔵⭕🔵🔵🔵🔵🔵🔵🔵\n" +
                "🔵⬛🔵⬛⭕⬛⭕🕔⭕\n" +
                "🔵⭕🔵🔵🔵⭕⭕⭕🔺\n" +
                "🔵⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "🔵🔵🔵🔵🔵🔵🔵🔵😛"
        String text2 = '' +
                'Score 260\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '🔵⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛🔵⬛⭕⬛⭕🕛⭕\n' +
                '🔵⭕🔵🔵🔵⭕🔺⭕😛\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵'
        String text3 = '' +
                'Score 480\n' +
                '\n' +
                '💚💚💚💚💚💚💚💚💚\n' +
                '💚⬛⭕⬛⭕⬛⭕⬛💚\n' +
                '💚⭕💚💚💚💚💚💚💚\n' +
                '💚🇬💚🇴⭕🇴⭕🇩⭕\n' +
                '💚⭕💚💚💚💚💚💚💚\n' +
                '💚🇬⭕🇦⭕🇲⭕🇪💚\n' +
                '💚⭕💚💚💚💚💚💚💚\n' +
                '💚⬛💚⬛⭕⬛⭕⬛⭕\n' +
                '💚⭕💚💚💚⭕😃💚💚\n' +
                '💚⬛⭕⬛⭕⬛⭕⬛💚\n' +
                '💚💚💚💚💚💚💚💚💚'
        Status tweet1 = Mock()
        Status tweet2 = Mock()
        Status tweet3 = Mock()
        Date createdAt = Date.from(ZonedDateTime.parse('2019-06-20T06:28:34Z').toInstant())

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >>
                '{"thm":"TEST","thr":31,"sn":[10,6,"L6U10R8D2L6D2R6D2L6D2R4",6],' +
                '"tg":[8,8],"sc":210,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        2 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(text1, 1234567890, null) >> tweet1
        1 * tweet1.getId() >> 87654L
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(text2, 87654L, null) >> tweet2
        1 * tweet2.getId() >> 87655L
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(text3, 87655L, null) >> tweet3
        4 * tweet3.getId() >> 87656L
        1 * database.readPlaythroughAndScores() >>
                new PlaythroughAndScores('11-1141599440618192897-2019062006-b', null)
        1 * tweet3.getCreatedAt() >> createdAt
        1 * database.deleteStateAndUpdateScores('11b480-3m?;l4pTLS-wg3*:-b5)-wg3*:')
        1 * pollMaker.tweet('High Scores:\n\n* 1. 480 #11 🍞 0h *', 87656, null)
        0 * _._
        outcome.tweets == [tweet1, tweet2, tweet3]
        outcome.previousGameString == previous
    }

    def "should start new twitter thread when database starts empty"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(22)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        Status tweet = Mock()
        String gameText = '' +
                'Score 0\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕🕛⭕\n' +
                '⭕⭕⭕⭕⭕⭕🔺⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕😋⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        String gameState = '{"sc":0,"tg":[2,6],"thm":"TEST","sn":[8,4,"U2",0],' +
                '"id":1130374678915141634,"thr":1}'

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >> null
        1 * database.readPlaythrough() >> '9876-1128980514847698944-2019051705-f'
        1 * pollMaker.readPreviousTweet(null) >> null
        1 * pollMaker.postPoll(20, gameText, ['⬅️ Left', '➡️ Right', '⬇️ Down'], null) >> tweet
        1 * database.writeStateAndPlaythrough(gameState, '9877-1130374678915141634-2019052419-s')
        3 * tweet.getId() >> 1130374678915141634L
        1 * tweet.getCreatedAt() >>
                Date.from(ZonedDateTime.parse('2019-05-24T19:45:30Z').toInstant())
        0 * _._
        outcome.tweets == [tweet]
    }

    def "should continue thread when database has game state, with super majority"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 1),
                new Choice("➡️ Right", 50), // Super majority
                new Choice("⬇️ Down", 1)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameString = '' +
                'Score 40\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵🔵🔵🔵🔵\n' +
                '⭕⬛⭕🕚🔵⬛⭕⬛⭕\n' +
                '⭕⭕🔺⭕😛⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        String nextGameString = '' +
                'Score 40\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕🔵🔵🔵⭕⭕\n' +
                '⭕⬛⭕🕙🔵⬛⭕⬛⭕\n' +
                '⭕⭕🔺⭕🔵🔵🙂⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        String previousGameState = '{"sc":40,"tg":[8,2],"thm":"TEST","sn":[8,4,"U2R4",1],' +
                '"id":5678,' +
                '"thr":31}'
        String newGameState = '{"sc":40,"tg":[8,2],"thm":"TEST","sn":[8,6,"L2U2R2",2],"id":87654,' +
                '"thr":32}'

        Status tweet = Mock()

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >> previousGameState
        1 * pollMaker.readPreviousTweet(5678L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        2 * previousTweet.getId() >> 5678L
        1 * pollMaker.postPoll(20, nextGameString,
                ['⬆️ Up', '➡️ Right', '⬇️ Down'], 5678L) >> tweet
        2 * tweet.getId() >> 87654L
        1 * database.writeGameState(newGameState)
        outcome.previousGameString == previousGameString
        outcome.tweets == [tweet]
        0 * _._
    }

    def 'should start new thread after thread gets long'() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        StatusWithCard previousGameTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 30), // Super majority
                new Choice("➡️ Right", 0),
                new Choice("⬇️ Down", 0)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameState = '{"sc":40,"tg":[8,2],"thm":"TEST","sn":[8,4,"U2R4U2L6U2R6",4],' +
                '"id":987654321,"thr":57}'
        String newGameState = '{"sc":120,"tg":[6,2],"thm":"TEST","sn":[8,2,"R2U2R4U2L6U2R6",0],' +
                '"id":87654,"thr":1}'
        String previousGameString = '' +
                'Score 40\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '⭕⭕⭕⭕🔵🔵🔵🔵🔵\n' +
                '⭕⬛⭕🕗🔵⬛⭕⬛⭕\n' +
                '⭕⭕🔺⭕😛⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        Status newGameTweet = Mock()
        String newGameString = '' +
                'Score 120\n' +
                '\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕🔵🔵🔵🔵🔵🔵🔵\n' +
                '⭕⬛⭕🕛⭕⬛⭕⬛🔵\n' +
                '⭕⭕🔺⭕🔵🔵🔵🔵🔵\n' +
                '⭕⬛⭕⬛🔵⬛⭕⬛⭕\n' +
                '⭕⭕😛🔵🔵⭕⭕⭕⭕\n' +
                '⭕⬛⭕⬛⭕⬛⭕⬛⭕\n' +
                '⭕⭕⭕⭕⭕⭕⭕⭕⭕'
        Status introTweet = Mock()
        Status outroTweet = Mock()
        long previousGameTweetId = 987654321L
        long introTweetId = 898989898L

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >> previousGameState
        1 * pollMaker.readPreviousTweet(previousGameTweetId) >> previousGameTweet
        2 * previousGameTweet.getCard() >> previousPoll
        3 * previousGameTweet.getId() >> previousGameTweetId
        1 * pollMaker.tweet('Continuing game from thread… https://twitter' +
                '.com/EmojiTetraGamma/status/987654321', null, null) >> introTweet
        1 * introTweet.getId() >> introTweetId
        1 * time.pauseBriefly()
        1 * pollMaker.postPoll(20, newGameString, ['⬆️ Up', '⬅️ Left', '⬇️ Down'], introTweetId) >>
                newGameTweet
        1 * time.pauseBriefly()
        1 * pollMaker.tweet('Game continues in new thread… https://twitter' +
                '.com/EmojiTetraGamma/status/87654', previousGameTweetId, null) >> outroTweet
        3 * newGameTweet.getId() >> 87654L
        1 * database.writeGameState(newGameState)
        outcome.previousGameString == previousGameString
        0 * _._
    }

    @Unroll
    def 'should extend long thread with intermediary tweets with game over '() {

        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        StatusWithCard previousGameTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice('⬆️ Up', 1),
                new Choice('⬅️ Left', 30) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameState = '{"sc":240,"tg":[0,2],"thm":"TEST","sn":[2,2,' +
                '"D2L2D2R2D2L2D2R4U8R2D8R2U10L2",4],"id":987654321,"thr":' + threadLength + '}'

        String previousGameString = '' +
                'Score 240\n' +
                '\n' +
                '⭕⭕🔺⭕⭕⭕🔵🔵🔵\n' +
                '⭕⬛⭕🕗⭕⬛⭕⬛🔵\n' +
                '⭕⭕😛⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵🔵🔵'
        // thread length 47

        long inter1Id = 11111L
        Status inter1Tweet = Mock()
        String inter1 = '' +
                'Score 240\n' +
                '\n' +
                '⭕⭕🔺⭕⭕⭕⭕⭕🔵\n' +
                '⭕⬛⭕🕖⭕⬛⭕⬛🔵\n' +
                '😐🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵🔵🔵'
        // thread length 48

        long inter2Id = 22222L
        Status inter2Tweet = Mock()
        String inter2 = '' +
                'Score 240\n' +
                '\n' +
                '😛⭕🔺⭕⭕⭕⭕⭕⭕\n' +
                '🔵⬛⭕🕕⭕⬛⭕⬛⭕\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵🔵🔵'
        // thread length 49

        long inter3Id = 33333L
        Status inter3Tweet = Mock()
        String inter3 = '' +
                'Score 300\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵😛⭕🔺\n' +
                '🔵⬛⭕⬛⭕⬛⭕🕙⭕\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕⭕\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕⭕\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵🔵🔵'
        // thread length 50, but keep going until game ends or new poll happens

        long inter4Id = 44444L
        Status inter4Tweet = Mock()
        String inter4 = '' +
                'Score 400\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕😛\n' +
                '⭕⬛🔵⬛🔵⬛🔵🕚⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔺\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕⭕\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵🔵🔵'

        long inter5Id = 55555L
        Status inter5Tweet = Mock()
        String inter5 = '' +
                'Score 510\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕😛\n' +
                '🔵⬛⭕⬛🔵⬛🔵🕛⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔺\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛⭕\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵🔵🔵'

        long goodGameId = 60006493L
        Status goodGameTweet = Mock()
        String goodGameString = '' +
                'Score 730\n' +
                '\n' +
                '💚💚💚💚💚💚💚💚💚\n' +
                '💚⬛⭕⬛⭕⬛⭕⬛💚\n' +
                '💚💚💚⭕💚💚💚⭕💚\n' +
                '⭕🇬💚🇴💚🇴💚🇩💚\n' +
                '💚💚💚⭕💚⭕💚⭕💚\n' +
                '💚🇬⭕🇦💚🇲💚🇪💚\n' +
                '💚💚💚⭕💚⭕💚⭕😃\n' +
                '⭕⬛💚⬛💚⬛💚⬛⭕\n' +
                '💚💚💚⭕💚⭕💚⭕💚\n' +
                '💚⬛⭕⬛💚⬛💚⬛💚\n' +
                '💚💚💚💚💚⭕💚💚💚'

        long previousGameTweetId = 987654321L
        Date createdAt = Date.from(ZonedDateTime.parse('2019-06-20T06:28:34Z').toInstant())

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >> previousGameState
        1 * pollMaker.readPreviousTweet(previousGameTweetId) >> previousGameTweet
        2 * previousGameTweet.getCard() >> previousPoll
        2 * previousGameTweet.getId() >> previousGameTweetId
        1 * pollMaker.tweet(inter1, previousGameTweetId, null) >> inter1Tweet
        1 * inter1Tweet.getId() >> inter1Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter2, inter1Id, null) >> inter2Tweet
        1 * inter2Tweet.getId() >> inter2Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter3, inter2Id, null) >> inter3Tweet
        1 * inter3Tweet.getId() >> inter3Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter4, inter3Id, null) >> inter4Tweet
        1 * inter4Tweet.getId() >> inter4Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter5, inter4Id, null) >> inter5Tweet
        1 * inter5Tweet.getId() >> inter5Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(goodGameString, inter5Id, null) >> goodGameTweet
        4 * goodGameTweet.getId() >> goodGameId
        1 * database.readPlaythroughAndScores() >>
                new PlaythroughAndScores('11-1141599440618192897-2019062006-b', null)
        1 * goodGameTweet.getCreatedAt() >> createdAt
        1 * database.deleteStateAndUpdateScores('11b730-3m?;l4pTLS-wg3*:-:aTn-wg3*:')
        1 * pollMaker.tweet('High Scores:\n\n* 1. 730 #11 🍞 0h *', 60006493, null)
        outcome.previousGameString == previousGameString
        outcome.tweets == [inter1Tweet, inter2Tweet, inter3Tweet, inter4Tweet, inter5Tweet,
                           goodGameTweet]
        0 * _._

        // Same result regardless of thread length, since this is game over, and long threads are
        // okay within reason.
        where:
        threadLength << [13, 47]
    }

    def 'should continue thread with intermediary tweets and new poll'() {

        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice('⬆️ Up', 199),
                new Choice('⬅️ Left', 1)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameState = '{"sc":200,"tg":[2,6],"thm":"TEST",' +
                '"sn":[4,6,"D6R2U10L8D2R2D2L2D2R4D2",1],"id":5678,"thr":31}'
        String previousGameText = '' +
                'Score 200\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕🕚🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔺⭕🔵\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕😛⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'
        long inter1Id = 1111L
        Status inter1Tweet = Mock()
        String inter1 = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕😋⭕🔵\n' +
                '⭕⬛🔵⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕛🔵⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'
        long inter2Id = 2222L
        Status inter2Tweet = Mock()
        String inter2 = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🙂🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕚⭕⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'
        long inter3Id = 3333L
        Status inter3Tweet = Mock()
        String inter3 = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🙂⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕙⭕⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'

        String newGameText = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵⭕⭕⭕🙂⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕘⭕⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'

        String newGameState = '{"sc":310,"tg":[8,2],"thm":"TEST",' +
                '"sn":[6,4,"U4R2D8R2U10L8D2R2D2L2D2",3],"id":87654,"thr":35}'

        Status newGameTweet = Mock()

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >> previousGameState
        1 * pollMaker.readPreviousTweet(5678L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        2 * previousTweet.getId() >> 5678L

        1 * pollMaker.tweet(inter1, 5678L, null) >> inter1Tweet
        1 * inter1Tweet.getId() >> inter1Id
        1 * time.pauseBriefly()

        1 * pollMaker.tweet(inter2, inter1Id, null) >> inter2Tweet
        1 * inter2Tweet.getId() >> inter2Id
        1 * time.pauseBriefly()

        1 * pollMaker.tweet(inter3, inter2Id, null) >> inter3Tweet
        1 * inter3Tweet.getId() >> inter3Id
        1 * time.pauseBriefly()

        1 * pollMaker.postPoll(20, newGameText, ['⬅️ Left', '⬇️ Down'], inter3Id) >> newGameTweet
        2 * newGameTweet.getId() >> 87654L
        1 * database.writeGameState(newGameState)
        outcome.previousGameString == previousGameText
        outcome.tweets == [inter1Tweet, inter2Tweet, inter3Tweet, newGameTweet]
        0 * _._
    }

    def 'should finish a thread with intermediary tweets and start new thread with new poll'() {

        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        long previousTweetId = 5678L
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice('⬆️ Up', 199),
                new Choice('⬅️ Left', 1)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameState = '{"sc":200,"tg":[2,6],"thm":"TEST",' +
                '"sn":[4,6,"D6R2U10L8D2R2D2L2D2R4D2",1],"id":5678,"thr":50}'
        String previousGameText = '' +
                'Score 200\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕🕚🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔺⭕🔵\n' +
                '⭕⬛🔵⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕😛⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'
        long inter1Id = 1111L
        Status inter1Tweet = Mock()
        String inter1 = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕😋⭕🔵\n' +
                '⭕⬛🔵⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕛🔵⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'
        long inter2Id = 2222L
        Status inter2Tweet = Mock()
        String inter2 = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🙂🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵🔵🔵⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕚⭕⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'
        long inter3Id = 3333L
        Status inter3Tweet = Mock()
        String inter3 = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🙂⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕙⭕⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'

        String newGameText = '' +
                'Score 310\n' +
                '\n' +
                '🔵🔵🔵🔵🔵🔵🔵🔵🔵\n' +
                '🔵⬛⭕⬛⭕⬛⭕⬛🔵\n' +
                '🔵🔵🔵⭕🔵🔵🔵⭕🔵\n' +
                '⭕⬛🔵⬛🔵⬛🔵⬛🔵\n' +
                '🔵🔵🔵⭕🔵⭕🔵⭕🔵\n' +
                '🔵⬛⭕⬛🔵⬛🔵⬛🔵\n' +
                '🔵⭕⭕⭕🙂⭕🔵⭕🔵\n' +
                '⭕⬛⭕🕘⭕⬛🔵⬛🔵\n' +
                '⭕⭕🔺⭕⭕⭕🔵⭕🔵\n' +
                '⭕⬛⭕⬛⭕⬛🔵⬛🔵\n' +
                '⭕⭕⭕⭕⭕⭕🔵🔵🔵'

        String newGameState = '{"sc":310,"tg":[8,2],"thm":"TEST",' +
                '"sn":[6,4,"U4R2D8R2U10L8D2R2D2L2D2",3],"id":87654,"thr":4}'
        Status newGameTweet = Mock()
        long newGameTweetId = 87654L

        Status introTweet = Mock()
        long introTweetId = 123456L

        Status outroTweet = Mock()

        when:
        Player player = new Player(random, time, environment, database, pollMaker, EmojiSet.TEST)
        Outcome outcome = player.play()

        then:
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        1 * database.readGameState() >> previousGameState
        1 * pollMaker.readPreviousTweet(5678L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        3 * previousTweet.getId() >> previousTweetId
        1 * pollMaker.tweet('Continuing game from thread… https://twitter.com/' +
                'EmojiTetraGamma/status/5678', null, null) >> introTweet
        1 * introTweet.getId() >> introTweetId
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter1, introTweetId, null) >> inter1Tweet
        1 * inter1Tweet.getId() >> inter1Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter2, inter1Id, null) >> inter2Tweet
        1 * inter2Tweet.getId() >> inter2Id
        1 * time.pauseBriefly()
        1 * pollMaker.tweet(inter3, inter2Id, null) >> inter3Tweet
        1 * inter3Tweet.getId() >> inter3Id
        1 * time.pauseBriefly()
        1 * pollMaker.postPoll(20, newGameText, ['⬅️ Left', '⬇️ Down'], inter3Id) >>
                newGameTweet
        3 * newGameTweet.getId() >> newGameTweetId
        1 * time.pauseBriefly()
        1 * pollMaker.tweet('Game continues in new thread… https://twitter.com/' +
                'EmojiTetraGamma/status/87654', previousTweetId, null) >> outroTweet
        1 * database.writeGameState(newGameState)
        outcome.previousGameString == previousGameText
        outcome.tweets == [inter1Tweet, inter2Tweet, inter3Tweet, newGameTweet]
        0 * _._
    }
}
