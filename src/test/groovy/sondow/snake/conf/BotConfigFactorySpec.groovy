package sondow.snake.conf

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification
import twitter4j.conf.Configuration

class BotConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration with environment variables"() {
        setup:
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        String filler = Environment.SPACE_FILLER
        envVars.set("cred_twitter_reading",
                "${filler}disney,mickey,donald,goofy,pluto${filler}")
        envVars.set("cred_twitter_writing",
                "${filler}${filler}looneytunes,bugs,daffy,porky,sylvester${filler}")
        envVars.set("cred_airtable_personal_access_token",
                "${filler}${filler}${filler}chewy${filler}${filler}${filler}")
        envVars.set("cred_airtable_base",
                "${filler}${filler}${filler}han${filler}${filler}${filler}")

        when:
        BotConfigFactory factory = new BotConfigFactory(environment)
        Configuration writingConfig = factory.configure().getWritingTwitterConfig()
        Configuration readingConfig = factory.configure().getReadingTwitterConfig()

        then:
        with(readingConfig) {
            user == 'disney'
            OAuthConsumerKey == 'mickey'
            OAuthConsumerSecret == 'donald'
            OAuthAccessToken == 'goofy'
            OAuthAccessTokenSecret == 'pluto'
        }
        with(writingConfig) {
            user == 'looneytunes'
            OAuthConsumerKey == 'bugs'
            OAuthConsumerSecret == 'daffy'
            OAuthAccessToken == 'porky'
            OAuthAccessTokenSecret == 'sylvester'
        }
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        4 * fileClerk.readTextFile(_) >> null // No encryption file
        0 * _._
    }
}
