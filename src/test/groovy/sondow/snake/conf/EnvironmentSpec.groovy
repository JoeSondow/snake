package sondow.snake.conf

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification

class EnvironmentSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "should get value from file"() {
        setup:
        envVars.set("twitter_access_id", "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZmoondoggy")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        when:
        String twitterConsumerId = environment.get("twitter_consumer_id")
        String twitterAccessId = environment.get("twitter_access_id")
        String twitterHandle = environment.get("twitter_handle")

        then:
        twitterConsumerId == "panda"
        twitterAccessId == "moondoggy"
        twitterHandle == "SchoolsOfFish"
        1 * keymaster.getCryptoKey('twitter_consumer_id') >> null
        1 * keymaster.getValue('twitter_consumer_id') >> "panda"
        1 * keymaster.getCryptoKey('twitter_access_id') >> null
        1 * keymaster.getCryptoKey('twitter_handle') >> null
        1 * keymaster.getValue("twitter_handle") >> "SchoolsOfFish"
        0 * _._
    }

    def "should get value and encryption key from file"() {
        setup:
        envVars.set("twitter_access_id", "5rosvtyjy")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        when:
        String twitterConsumerId = environment.get("twitter_consumer_id")
        String twitterAccessId = environment.get("twitter_access_id")
        String twitterHandle = environment.get("twitter_handle")
        String options = environment.get("options")

        then:
        twitterConsumerId == "panda"
        twitterAccessId == "moondoggy"
        twitterHandle == "SchoolsOfFish"
        options == "dracula"
        1 * keymaster.getCryptoKey('twitter_consumer_id') >> "wrerewqrqe"
        1 * keymaster.getValue('twitter_consumer_id') >> "Brrue"
        1 * keymaster.getCryptoKey('twitter_access_id') >> "sdafsfsdaf"
        1 * keymaster.getCryptoKey('twitter_handle') >> "czvzvxcvvcxvc"
        1 * keymaster.getValue("twitter_handle") >> "U23D09u)1H6Dj"
        1 * keymaster.getCryptoKey("options") >> null
        1 * keymaster.getValue("options") >> "dracula"
        0 * _._
    }

    def "Environment.get should strip prefix if present once"() {
        setup:
        envVars.set("twitter_access_id", "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZmoondoggy")
        envVars.set("twitter_handle", "SchoolsOfFish")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        expect:
        "moondoggy" == environment.get("twitter_access_id")
        "SchoolsOfFish" == environment.get("twitter_handle")
    }

    def "Environment.get should strip prefix if present twice"() {
        setup:
        envVars.set("twitter_access_id",
                "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZmoondoggy")
        envVars.set("twitter_handle", "SchoolsOfFish")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        expect:
        "moondoggy" == environment.get("twitter_access_id")
        "SchoolsOfFish" == environment.get("twitter_handle")
    }

    def "Environment.get should strip prefix if present thrice"() {
        setup:
        envVars.set("twitter_access_id",
                "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                        "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZmoondoggy")
        envVars.set("twitter_handle", "SchoolsOfFish")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        expect:
        "moondoggy" == environment.get("twitter_access_id")
        "SchoolsOfFish" == environment.get("twitter_handle")
    }

    def "Environment.get should strip prefix if present quarce"() {
        setup:
        envVars.set("twitter_access_id",
                "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                        "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                        "moondoggy")
        envVars.set("twitter_handle", "SchoolsOfFish")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        expect:
        "moondoggy" == environment.get("twitter_access_id")
        "SchoolsOfFish" == environment.get("twitter_handle")
    }

    def "Environment.get should use default value if variable not set"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        expect:
        "sunny" == environment.get("nothing_here", "sunny")
    }

    def "getInt should return default value if variable not set"() {
        setup:
        envVars.set("width", "1400")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        expect:
        1400 == environment.getInt("width", 800)
        600 == environment.getInt("height", 600)
    }

    def "getInt should throw NumberFormatException if value is not an integer"() {
        setup:
        envVars.set("width", "wide")
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)

        when:
        environment.getInt("width", 800)

        then:
        thrown NumberFormatException
    }
}
