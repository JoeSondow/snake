package sondow.snake.io

import sondow.snake.game.GameSummary
import spock.lang.Specification

class AbridgerSpec extends Specification {

    def 'should abridge game summary lists to 5 or fewer unless too many targets eaten'() {
        setup:
        List<GameSummary> gameSummaries = input.collect {
            new GameSummary('The text is irrelevant here', it)
        }
        List<GameSummary> expected = output.collect {
            new GameSummary('The text is irrelevant here', it)
        }
        Abridger abridger = new Abridger()

        expect:
        abridger.abridge(gameSummaries) == expected

        where:
        output                   | input
        []                       | []
        [0]                      | [0]
        [7]                      | [7]
        [7, 8, 0, 1]             | [7, 8, 0, 1]
        [11, 12, 13, 0, 1]       | [11, 12, 13, 0, 1]
        [11, 13, 0, 1, 2]        | [11, 12, 13, 0, 1, 2]
        [1, 2, 3, 4, 5]          | [1, 2, 3, 4, 5]
        [1, 3, 4, 5, 6]          | [1, 2, 3, 4, 5, 6]
        [1, 3, 4, 6, 7]          | [1, 2, 3, 4, 5, 6, 7]
        [1, 3, 5, 7, 8]          | [1, 2, 3, 4, 5, 6, 7, 8]
        [1, 3, 5, 7, 9]          | [1, 2, 3, 4, 5, 6, 7, 8, 9]
        [1, 4, 6, 8, 10]         | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        [1, 4, 6, 9, 11]         | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        [1, 4, 7, 10, 12]        | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        [1, 4, 7, 10, 13]        | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        [1, 5, 8, 11, 14]        | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        [1, 5, 8, 12, 15]        | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        [1, 5, 9, 13, 16]        | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
        [1, 5, 9, 13, 17]        | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        [1, 6, 10, 14, 18]       | (1..18).collect { it }
        [1, 6, 10, 15, 19]       | (1..19).collect { it }
        [1, 6, 11, 16, 20]       | (1..20).collect { it }
        [1, 6, 11, 16, 21]       | (1..21).collect { it }
        [1, 7, 12, 17, 22]       | (1..22).collect { it }
        [1, 7, 12, 18, 23]       | (1..23).collect { it }
        [1, 7, 13, 19, 24]       | (1..24).collect { it }
        [1, 7, 13, 19, 25]       | (1..25).collect { it }
        [1, 8, 14, 20, 26]       | (1..26).collect { it }
        [1, 8, 14, 21, 27]       | (1..27).collect { it }
        [1, 8, 15, 22, 28]       | (1..28).collect { it }
        [1, 8, 15, 22, 29]       | (1..29).collect { it }
        [1, 9, 16, 23, 30]       | (1..30).collect { it }
        [1, 9, 16, 24, 31]       | (1..31).collect { it }
        [4, 1, 4, 2, 5, 1, 0, 0] | [4, 0, 1, 0, 1, 2, 3, 4, 0, 1, 2, 0, 1, 2, 3, 4, 5, 0, 1, 0, 0]
        [0, 1, 4, 2, 5, 1]       | [0, 1, 0, 1, 2, 3, 4, 0, 1, 2, 0, 1, 2, 3, 4, 5, 0, 1]
    }

    def 'should add middle number to widest gap'() {
        setup:
        Abridger abridger = new Abridger()

        when:
        TreeSet<Integer> indicesSet = new TreeSet<>(indicesList)
        abridger.addMiddleNumberToWidestGap(indicesSet)

        then:
        new ArrayList<Integer>(indicesSet) == result

        where:
        indicesList     | result
        []              | []
        [0]             | [0]
        [5]             | [5]
        [0, 1]          | [0, 1]
        [0, 2]          | [0, 1, 2]
        [0, 3]          | [0, 2, 3]
        [0, 4]          | [0, 2, 4]
        [0, 6]          | [0, 3, 6]
        [0, 7]          | [0, 4, 7]
        [0, 4, 7]       | [0, 2, 4, 7]
        [0, 2, 4, 7]    | [0, 2, 4, 6, 7]
        [0, 28]         | [0, 14, 28]
        [0, 14, 28]     | [0, 14, 21, 28]
        [0, 14, 22, 28] | [0, 7, 14, 22, 28]
        [0, 3, 35]      | [0, 3, 19, 35]
        [0, 3, 19, 35]  | [0, 3, 19, 27, 35]
        [0, 3, 29, 35]  | [0, 3, 16, 29, 35]
        [0, 3, 34]      | [0, 3, 19, 34]
        [0, 3, 34]      | [0, 3, 19, 34]
        [0, 3, 19, 34]  | [0, 3, 11, 19, 34]
    }
}
