package sondow.snake.io

import java.time.ZonedDateTime
import sondow.snake.game.Game
import sondow.snake.game.GameRecord
import sondow.snake.game.Playthrough
import sondow.snake.game.Snake
import sondow.snake.game.Target
import spock.lang.Specification

class ConverterSpec extends Specification {

    def 'should convert database string v2 with atomicMovesSinceEating to game'() {

        setup:
        String contents = '{"thm":"TEST","thr":31,"sn":[0,6,"L2D4L4U4R2D2",7],"tg":[4,8],' +
                '"sc":80,"id":1234567890}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 80\n" +
                "\n" +
                "🔵🔵🔵⭕🔵🔵😐⭕⭕\n" +
                "🔵⬛🔵⬛🔵⬛⭕⬛⭕\n" +
                "🔵⭕🔵⭕🔵⭕⭕⭕⭕\n" +
                "🔵⬛⭕⬛🔵⬛⭕🕔⭕\n" +
                "🔵🔵🔵🔵🔵⭕⭕⭕🔺\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
        Snake expectedSnake = new Snake().addKeyPoint(0, 6).addKeyPoint(0, 4).addKeyPoint(4, 4).
                addKeyPoint(4, 0).addKeyPoint(0, 0).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(7)
        Target expectedTarget = new Target(4, 8)

        when:
        Game game = converter.stateStringToGame(contents)

        then:
        game.threadLength == 31
        game.toString() == expected
        game.score == 80
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1234567890L
    }

    def 'should convert database string v1 without atomicMovesSinceEating to game'() {

        setup:
        String contents = '{"thm":"TEST","thr":31,"sn":[0,6,"L2D4L4U4R2D2"],"tg":[4,8],' +
                '"sc":80,"id":1234567890}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 80\n" +
                "\n" +
                "🔵🔵🔵⭕🔵🔵😋⭕⭕\n" +
                "🔵⬛🔵⬛🔵⬛⭕⬛⭕\n" +
                "🔵⭕🔵⭕🔵⭕⭕⭕⭕\n" +
                "🔵⬛⭕⬛🔵⬛⭕🕛⭕\n" +
                "🔵🔵🔵🔵🔵⭕⭕⭕🔺\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
        Snake expectedSnake = new Snake().addKeyPoint(0, 6).addKeyPoint(0, 4).addKeyPoint(4, 4).
                addKeyPoint(4, 0).addKeyPoint(0, 0).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(0)
        Target expectedTarget = new Target(4, 8)

        when:
        Game game = converter.stateStringToGame(contents)

        then:
        game.threadLength == 31
        game.toString() == expected
        game.score == 80
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1234567890L
    }

    def 'should convert database string with a double-digit number to game'() {

        setup:
        String contents = ' {"sc":90,"tg":[10,0],"thm":"TEST","sn":[10,6,"R2U10L4D4",1],' +
                '"id":1098947596419133441,"thr":34}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 90\n" +
                "\n" +
                "⭕⭕⭕⭕🔵🔵🔵🔵🔵\n" +
                "⭕⬛⭕⬛🔵⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕🔵⭕⭕⭕🔵\n" +
                "⭕⬛⭕⬛🔵⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕🔵⭕⭕⭕🔵\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕🔵\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛🔵\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕🔵\n" +
                "⭕🕚⭕⬛⭕⬛⭕⬛🔵\n" +
                "🔺⭕⭕⭕⭕⭕🙂🔵🔵"
        Snake expectedSnake = new Snake().addKeyPoint(10, 6).addKeyPoint(10, 8).
                addKeyPoint(0, 8).addKeyPoint(0, 4).addKeyPoint(4, 4).
                setAtomicMovesSinceEating(1)
        Target expectedTarget = new Target(10, 0)

        when:
        Game game = converter.stateStringToGame(contents)

        then:
        game.threadLength == 34
        game.toString() == expected
        game.score == 90
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1098947596419133441L
    }

    def 'should convert database string with many double-digit numbers to game'() {

        setup:
        String contents = ' {"sc":90,"tg":[10,0],"thm":"TEST",' +
                '"sn":[10,8,"U10L2D10L2U10L2D10",902],"id":1098947596419133441,"thr":34}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "Score 90\n" +
                "\n" +
                "⭕⭕🔵🔵🔵⭕🔵🔵🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕⬛🔵⬛🔵⬛🔵⬛🔵\n" +
                "⭕⭕🔵⭕🔵⭕🔵⭕🔵\n" +
                "⭕🚫🔵⬛🔵⬛🔵⬛🔵\n" +
                "🔺⭕🔵⭕🔵🔵🔵⭕😟"
        Snake expectedSnake = new Snake().addKeyPoint(10, 8).addKeyPoint(0, 8).
                addKeyPoint(0, 6).addKeyPoint(10, 6).addKeyPoint(10, 4).
                addKeyPoint(0, 4).addKeyPoint(0, 2).addKeyPoint(10, 2).
                setAtomicMovesSinceEating(902)
        Target expectedTarget = new Target(10, 0)

        when:
        Game game = converter.stateStringToGame(contents)

        then:
        game.threadLength == 34
        game.toString() == expected
        game.score == 90
        game.snake == expectedSnake
        game.target == expectedTarget
        game.tweetId == 1098947596419133441L
    }

    def 'should roundtrip convert game to database string and then back into game etc'() {

        setup:
        Random random = new Random(4)
        Converter converter = new Converter(random)
        Game game = new Game(EmojiSet.TEST, random)
        Snake snake = new Snake().addKeyPoint(0, 6).addKeyPoint(0, 4).addKeyPoint(4, 4).
                addKeyPoint(4, 0).addKeyPoint(0, 0).addKeyPoint(0, 2).addKeyPoint(2, 2).
                setAtomicMovesSinceEating(15)
        Target target = new Target(4, 8)
        game.setSnake(snake).setTarget(target).setScore(200).setThreadLength(15).
                setTweetId(1234567890L)

        when:
        String databaseFormat = converter.gameToStateString(game)
        Game newGame = converter.stateStringToGame(databaseFormat)
        String newDatabaseFormat = converter.gameToStateString(newGame)

        then:
        databaseFormat == '{"sc":200,"tg":[4,8],"thm":"TEST",' +
                '"sn":[0,6,"L2D4L4U4R2D2",15],"id":1234567890,"thr":15}'
        game == newGame
        databaseFormat == newDatabaseFormat
        game.toString() == "" +
                "Score 200\n" +
                "\n" +
                "🔵🔵🔵⭕🔵🔵😟⭕⭕\n" +
                "🔵⬛🔵⬛🔵⬛⭕⬛⭕\n" +
                "🔵⭕🔵⭕🔵⭕⭕⭕⭕\n" +
                "🔵⬛⭕⬛🔵⬛⭕🚫⭕\n" +
                "🔵🔵🔵🔵🔵⭕⭕⭕🔺\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕\n" +
                "⭕⬛⭕⬛⭕⬛⭕⬛⭕\n" +
                "⭕⭕⭕⭕⭕⭕⭕⭕⭕"
    }

    def 'should convert playthrough to database string and back to playthrough'() {
        setup:
        Playthrough playthrough = new Playthrough(9876L, 1128980514847698944L,
                ZonedDateTime.parse('2019-05-17T05:45:00Z'), [EmojiSet.FORTUNE])

        Converter converter = new Converter()

        when:
        String dbString = converter.playthroughToString(playthrough)
        Playthrough result = converter.stringToPlaythrough(dbString)

        then:
        dbString == '9876-1128980514847698944-2019051705-f'
        result == playthrough
    }

    def 'should convert zero game records to database string'() {
        setup:
        Converter converter = new Converter()

        when:
        String csv = converter.gameRecordsToString([])

        then:
        csv == ''
    }

    def 'should convert one game record to database string'() {
        setup:
        GameRecord record = new GameRecord(22,
                Arrays.asList(EmojiSet.PEACH), 2390, 1125713967312064517L,
                ZonedDateTime.parse('2019-05-07T11:48:00Z'),
                1126192190235893760L, ZonedDateTime.parse('2019-05-08T19:28:00Z'))

        Converter converter = new Converter()

        when:
        String csv = converter.gameRecordsToString([record])

        then:
        csv == '22p2390-3i:RrS&WqQ-wg2F4-3j7AOu/XY%-wg2Gn'
        // uncompressed: '22p2390-1125713967312064517-2019050711-1126192190235893760-2019050819'
    }

    ZonedDateTime d(String isoFormattedDate) {
        ZonedDateTime.parse(isoFormattedDate)
    }

    def 'should convert game records to database string roundtrip'() {
        setup:
        GameRecord record1 = new GameRecord(20, Arrays.asList(EmojiSet.FORTUNE), 2710,
                1122417245152395265L, d("2019-04-28T09:28:00Z"),
                1122809882191654912L, d("2019-04-29T11:28:00Z"))
        GameRecord record2 = new GameRecord(21, Arrays.asList(EmojiSet.DONUT), 2420,
                1125241102066262016L, d("2019-05-06T04:29:00Z"),
                1125703967772528641L, d("2019-05-07T11:08:00Z"))
        GameRecord record3 = new GameRecord(22, Arrays.asList(EmojiSet.MOON), 2140,
                1116201554917257216L, d("2019-04-11T05:49:00Z"),
                1116578838786400257L, d("2019-04-12T06:48:00Z"))
        GameRecord record4 = new GameRecord(23, Arrays.asList(EmojiSet.BREAD), 2580,
                1124385445586976768L, d("2019-05-03T19:49:00Z"),
                1124828219545272321L, d("2019-05-05T01:08:00Z"))
        GameRecord record5 = new GameRecord(24, Arrays.asList(EmojiSet.DONUT), 2650,
                1119422510938804224L, d("2019-04-20T03:08:00Z"),
                1119795010457571329L, d("2019-04-21T03:48:00Z"))

        List<GameRecord> records = Arrays.asList(record1, record2, record3, record4, record5)
        Converter converter = new Converter()

        when:
        String csv = converter.gameRecordsToString(records)
        List<GameRecord> newRecords = converter.stringToGameRecords(csv)
        String newCsv = converter.gameRecordsToString(newRecords)

        then:
        csv == '20f2710-3ib5@Z_GM}-wg1Fn-3ij{^RYuM$-wg1GA,21d2420-3i$)' +
                '$u%aPp-wg2D%-3i:xg*}+a?-wg2F4,22m2140-3gML+I&bPt-wg1ma-3gV66}u8yd-wg1nm,' +
                '23b2580-3iTObw[~Cj-wg2AV-3i]P4^29g&-wg2CZ,' +
                '24d2650-3hwv<c11Kb-wg1wi-3hE/H~B<P_-wg1xt'

        //noinspection ChangeToOperator because GameRecord has a limited compareTo method
        newRecords.equals(records)
        newCsv == csv
    }

    def 'should compress and expand big numbers like tweet ids in base 89'() {
        setup:
        Converter converter = new Converter()

        when:
        long id = base10
        String compressed = converter.compressInteger(id)
        long expanded = converter.expandCompressedInteger(compressed)

        then:
        compressed == base89
        expanded == id

        where:
        base10               | base89
        0L                   | '0'
        62L                  | "`"
        192488934L           | "36481"
        1125195551513776128L | "3i#*g8^iVZ"
        1125281117949419520L | "3i%@vD21Nu"
        20190516L            | "sU>]"
        2019051609L          | "wg2Pc"
    }
}
