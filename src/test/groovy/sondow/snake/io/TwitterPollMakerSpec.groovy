package sondow.snake.io

import spock.lang.Specification
import twitter4j.CardLight
import twitter4j.CardsTwitterImpl
import twitter4j.Status
import twitter4j.StatusUpdate
import twitter4j.StatusUpdateWithCard
import twitter4j.TwitterException
import twitter4j.User
import twitter4j.conf.Configuration

class TwitterPollMakerSpec extends Specification {

    def 'should tweet non-poll message if no error'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl)
        StatusUpdate statusUpdate = new StatusUpdate("Hello")
        Status status = Mock()

        when:
        twitterPollMaker.tweet("Hello", null, null)

        then:
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> status
        0 * _._
    }

    def 'should tweet non-poll message with braille blank appended if duplicate'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl)
        StatusUpdate statusUpdate = new StatusUpdate("Hello")
        StatusUpdate statusUpdateWithBrailleBlank = new StatusUpdate("Hello⠀")
        TwitterException dupeException = new TwitterException('' +
                '{"errors":[{"message":"Status is a duplicate.","code":999}]}')
        Status status = Mock()

        when:
        twitterPollMaker.tweet("Hello", null, null)

        then:
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> { throw dupeException }
        1 * writingCardsTwitterImpl.updateStatus(statusUpdateWithBrailleBlank) >> status
        0 * _._
    }

    def 'should tweet non-poll message with two braille blanks if one was already done'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl)
        StatusUpdate statusUpdate = new StatusUpdate("Hello")
        StatusUpdate statusUpdateWithBrailleBlank = new StatusUpdate("Hello⠀")
        StatusUpdate statusUpdateWithTwoBrailleBlanks = new StatusUpdate("Hello⠀⠀")
        TwitterException dupeException = new TwitterException('' +
                '{"errors":[{"message":"Status is a duplicate.","code":999}]}')
        Status status = Mock()

        when:
        twitterPollMaker.tweet("Hello", null, null)

        then:
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> { throw dupeException }
        1 * writingCardsTwitterImpl.updateStatus(statusUpdateWithBrailleBlank) >>
                { throw dupeException }
        1 * writingCardsTwitterImpl.updateStatus(statusUpdateWithTwoBrailleBlanks) >> status
        0 * _._
    }

    def 'should tweet poll if no error'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl)
        StatusUpdateWithCard statusUpdate = new StatusUpdateWithCard("Hello", 'some_card_uri')
        statusUpdate.inReplyToStatusId = 349L
        Status status = Mock()
        CardLight card = Mock()
        Map<String, String> params = ['twitter:api:api:endpoint'     : '1',
                                      'twitter:card'                 : 'poll2choice_text_only',
                                      'twitter:long:duration_minutes': '20',
                                      'twitter:string:choice1_label' : 'Red',
                                      'twitter:string:choice2_label' : 'Blue']

        when:
        twitterPollMaker.postPoll(20, "Hello", ['Red', 'Blue'], 349L)

        then:
        1 * writingCardsTwitterImpl.createCard(params) >> card
        1 * card.getCardUri() >> 'some_card_uri'
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> status
        0 * _._
    }

    static String longBio = 'I am a bot created by @JoeSondow. Vote on the next move. Not to be ' +
            'confused with any trademarked block-fitting game. #TeamPlummet #TeamRotate. High ' +
            'Score: 13850'
    static String longBio15k = 'I am a bot created by @JoeSondow. Vote on the next move. Not to ' +
            'be confused with any trademarked block-fitting game. #TeamPlummet #TeamRotate. High ' +
            'Score: 15000'

    def "should update bio with new high score"() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl)
        User user = Mock()
        String url = 'gitlab.com/emojitetra'

        when:
        twitterPollMaker.updateBioForScore(newScore)

        then:
        1 * writingConfig.getUser() >> 'Sammy'
        1 * writingCardsTwitterImpl.showUser('Sammy') >> user
        1 * user.getDescription() >> oldDesc
        updates * user.getLocation() >> null
        updates * user.getURL() >> url
        updates * user.getScreenName() >> 'Sammy'
        updates * writingCardsTwitterImpl.updateProfile('Sammy', url, null, newDesc)
        0 * _._

        where:
        newScore | updates | oldDesc                  | newDesc
        230      | 1       | 'Wassup High Score: 100' | 'Wassup High Score: 230'
        90       | 0       | 'Wassup High Score: 100' | 'Wassup High Score: 100'
        90       | 0       | 'Wassup High Score: 100' | 'Wassup High Score: 100'
        1400     | 0       | 'I love you'             | 'I love you'
        15000    | 1       | longBio                  | longBio15k
    }
}
