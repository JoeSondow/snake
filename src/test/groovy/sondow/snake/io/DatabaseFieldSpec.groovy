package sondow.snake.io

import spock.lang.Specification

class DatabaseFieldSpec extends Specification {

    def 'database field enum values should have correct lowercase field name strings'() {
        expect:
        DatabaseField.STATE.getFieldName() == 'state'
        DatabaseField.PLAYTHROUGH.getFieldName() == 'playthrough'
        DatabaseField.SCORES.getFieldName() == 'scores'
    }

    def 'database field enum values should invoke correct getters on record object'() {
        setup:
        AirtableRecord record = new AirtableRecord(state: 'yo', playthrough: 'hi', scores: 'sup')

        expect:
        DatabaseField.STATE.getFieldGetter().apply(record) == 'yo'
        DatabaseField.PLAYTHROUGH.getFieldGetter().apply(record) == 'hi'
        DatabaseField.SCORES.getFieldGetter().apply(record) == 'sup'
    }

    def 'database field enum values should invoke correct setters on record object'() {
        setup:
        AirtableRecord record = new AirtableRecord(state: 'yo', playthrough: 'hi', scores: 'sup')

        when:
        DatabaseField.STATE.getFieldSetter().accept(record, 'xatu')
        DatabaseField.PLAYTHROUGH.getFieldSetter().accept(record, 'abra')
        DatabaseField.SCORES.getFieldSetter().accept(record, 'uxie')

        then:
        record == new AirtableRecord(state: 'xatu', playthrough: 'abra', scores: 'uxie')
    }
}
