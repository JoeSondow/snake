package sondow.snake.io

import com.sybit.airtable.Airtable
import com.sybit.airtable.Base
import com.sybit.airtable.Query
import com.sybit.airtable.Table
import sondow.snake.conf.BotConfig
import sondow.snake.game.PlaythroughAndScores
import spock.lang.Specification
import spock.lang.Unroll
import twitter4j.conf.Configuration

class AirtableDatabaseSpec extends Specification {

    @Unroll("on state read-write if result is #queryResult then create count is #creationCount")
    def 'should read state from database and create record if necessary, then write new state'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableQuery('SchoolsOfFish', [DatabaseField.STATE])
        Database database = new AirtableDatabase(airtable, config)

        when:
        String gameState = database.readGameState()
        database.writeGameState('thenewgamestate')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord(name: 'SchoolsOfFish')]
        1 * table.update(new AirtableRecord(name: updatedName, state: 'thenewgamestate'))
        0 * _._
        gameState == expected

        where:
        queryResult                            | creationCount | expected  | updatedName
        [new AirtableRecord(state: 'E2J3d31')] | 0             | "E2J3d31" | null
        [new AirtableRecord(state: '')]        | 0             | null      | null
        [new AirtableRecord(state: null)]      | 0             | null      | null
        []                                     | 1             | null      | 'SchoolsOfFish'
    }

    @Unroll("on playthru read-write if result is #queryResult then create count is #creationCount")
    def 'should read playthru from db and create record if necessary, then write new playthru'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableQuery('SchoolsOfFish', [DatabaseField.PLAYTHROUGH])
        Database database = new AirtableDatabase(airtable, config)

        when:
        String playthrough = database.readPlaythrough()
        database.writePlaythrough('thenewgameplaythru')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord(name: 'SchoolsOfFish')]
        1 * table.update(new AirtableRecord(name: updatedName, playthrough: 'thenewgameplaythru'))
        0 * _._
        playthrough == expected

        where:
        queryResult                                  | creationCount | expected  | updatedName
        [new AirtableRecord(playthrough: 'E2J3d31')] | 0             | "E2J3d31" | null
        [new AirtableRecord(playthrough: '')]        | 0             | null      | null
        [new AirtableRecord(playthrough: null)]      | 0             | null      | null
        []                                           | 1             | null      | 'SchoolsOfFish'
    }

    @Unroll("on scores read-write if result is #queryResult then create count is #creationCount")
    def 'should read scores from db and create record if necessary, then write new scores'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableQuery('SchoolsOfFish', [DatabaseField.SCORES])
        Database database = new AirtableDatabase(airtable, config)

        when:
        String scores = database.readScores()
        database.writeScores('thenewgamescores')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord(name: 'SchoolsOfFish')]
        1 * table.update(new AirtableRecord(name: updatedName, scores: 'thenewgamescores'))
        0 * _._
        scores == expected

        where:
        queryResult                             | creationCount | expected  | updatedName
        [new AirtableRecord(scores: 'E2J3d31')] | 0             | "E2J3d31" | null
        [new AirtableRecord(scores: '')]        | 0             | null      | null
        [new AirtableRecord(scores: null)]      | 0             | null      | null
        []                                      | 1             | null      | 'SchoolsOfFish'
    }

    @Unroll("on db write if query result is #queryResult then creation count is #creationCount")
    def 'should write state to db even if record is not cached and create record if necessary'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableQuery('SchoolsOfFish', [DatabaseField.STATE])
        Database database = new AirtableDatabase(airtable, config)

        when:
        database.writeGameState('santa')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord(state: 'santa')]
        1 * table.update(new AirtableRecord(state: 'santa'))
        0 * _._

        where:
        queryResult                            | creationCount
        [new AirtableRecord(state: 'E2J3d31')] | 0
        [new AirtableRecord(state: '')]        | 0
        [new AirtableRecord(state: null)]      | 0
        []                                     | 1
    }

    def 'should read playthrough from database'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableQuery('SchoolsOfFish', [DatabaseField.PLAYTHROUGH])
        Database database = new AirtableDatabase(airtable, config)

        when:
        String playthrough = database.readPlaythrough()

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord()]
        0 * _._
        playthrough == result

        where:
        queryResult                                  | creationCount | result
        [new AirtableRecord(playthrough: 'E2J3d31')] | 0             | 'E2J3d31'
        [new AirtableRecord(playthrough: '')]        | 0             | null
        [new AirtableRecord(playthrough: null)]      | 0             | null
        []                                           | 1             | null
    }

    def 'should read scores from database'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        Query query = new AirtableQuery('SchoolsOfFish', [DatabaseField.SCORES])
        Database database = new AirtableDatabase(airtable, config)

        when:
        String scores = database.readScores()

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord()]
        0 * _._
        scores == result

        where:
        queryResult                             | creationCount | result
        [new AirtableRecord(scores: 'E2J3d31')] | 0             | 'E2J3d31'
        [new AirtableRecord(scores: '')]        | 0             | null
        [new AirtableRecord(scores: null)]      | 0             | null
        []                                      | 1             | null
    }

    def 'should read playthough and scores from database'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        List<DatabaseField> fields = [DatabaseField.PLAYTHROUGH, DatabaseField.SCORES]
        Query query = new AirtableQuery('SchoolsOfFish', fields)
        Database database = new AirtableDatabase(airtable, config)

        when:
        PlaythroughAndScores playthroughAndScores = database.readPlaythroughAndScores()

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        createcount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        createcount * table.select(query) >> [new AirtableRecord()]
        0 * _._
        playthroughAndScores.playthrough == playthru
        playthroughAndScores.scores == scores

        where:
        queryResult                                             | createcount | playthru | scores
        [new AirtableRecord(playthrough: 'hey', scores: 'sup')] | 0           | 'hey'    | 'sup'
        [new AirtableRecord(playthrough: '', scores: '')]       | 0           | null     | null
        [new AirtableRecord()]                                  | 0           | null     | null
        []                                                      | 1           | null     | null
    }

    def 'should write state and playthough to db'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        List<DatabaseField> fields = [DatabaseField.STATE, DatabaseField.PLAYTHROUGH]
        Query query = new AirtableQuery('SchoolsOfFish', fields)
        Database database = new AirtableDatabase(airtable, config)

        when:
        database.writeStateAndPlaythrough('dasher', 'reindeer')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord()]
        1 * table.update(new AirtableRecord(state: 'dasher', playthrough: 'reindeer'))
        0 * _._

        where:
        queryResult                                                 | creationCount
        [new AirtableRecord(state: 'bunny', playthrough: 'easter')] | 0
        [new AirtableRecord(state: '', playthrough: '')]            | 0
        [new AirtableRecord(state: null, playthrough: null)]        | 0
        []                                                          | 1
    }

    def 'should delete state and update scores'() {

        setup:
        Airtable airtable = Mock()
        BotConfig config = Mock()
        Base base = Mock()
        Table table = Mock()
        Configuration twitterConfig = Mock()
        List<DatabaseField> fields = [DatabaseField.STATE, DatabaseField.SCORES]
        Query query = new AirtableQuery('SchoolsOfFish', fields)
        Database database = new AirtableDatabase(airtable, config)

        when:
        database.deleteStateAndUpdateScores('reindeer')

        then:
        1 * config.getAirtableBaseId() >> '456abc'
        1 * airtable.base('456abc') >> base
        1 * base.table('SNAKE', AirtableRecord.class) >> table
        1 * config.getWritingTwitterConfig() >> twitterConfig
        1 * twitterConfig.getUser() >> 'SchoolsOfFish'
        1 * table.select(query) >> queryResult
        creationCount * table.create(new AirtableRecord(name: 'SchoolsOfFish'))
        creationCount * table.select(query) >> [new AirtableRecord()]
        1 * table.update(new AirtableRecord(state: '', scores: 'reindeer'))
        0 * _._

        where:
        queryResult                                            | creationCount
        [new AirtableRecord(state: 'bunny', scores: 'easter')] | 0
        [new AirtableRecord(state: 'bunny', scores: '')]       | 0
        [new AirtableRecord(state: 'bunny', scores: null)]     | 0
        [new AirtableRecord(state: '', scores: 'easter')]      | 0
        [new AirtableRecord(state: '', scores: '')]            | 0
        [new AirtableRecord(state: '', scores: null)]          | 0
        [new AirtableRecord(state: null, scores: 'easter')]    | 0
        [new AirtableRecord(state: null, scores: '')]          | 0
        [new AirtableRecord(state: null, scores: null)]        | 0
        []                                                     | 1
    }
}
