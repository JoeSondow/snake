package sondow.snake.io

import sondow.snake.game.FakeCard
import spock.lang.Specification
import spock.lang.Unroll
import twitter4j.Choice

class RefereeSpec extends Specification {

    @Unroll
    def "poll should have super majority iff 80%+ winner or 50%+ difference between winners"() {
        setup:
        Referee referee = new Referee()
        FakeCard card = new FakeCard()

        Choice left = new Choice("Left", lft)
        Choice right = new Choice("Right", rit)
        Choice upChoice = new Choice("Up", up)
        Choice down = new Choice("Down", dn)
        Choice[] choices = [left, right, upChoice, down].toArray(new Choice[0])
        card.setChoices(choices)

        when:
        boolean hasSuperMajority = referee.hasSuperMajority(card)

        then:
        hasSuperMajority == isSuperMajority

        where:
        lft | rit | up | dn | isSuperMajority
        0   | 0   | 0  | 0  | false
        10  | 70  | 0  | 20 | true
        0   | 30  | 70 | 0  | false
        5   | 15  | 80 | 0  | true
        5   | 5   | 90 | 0  | true
        30  | 1   | 69 | 0  | false
        10  | 10  | 69 | 11 | true
        50  | 0   | 40 | 10 | false
    }
}
