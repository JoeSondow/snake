package sondow.snake.conf;

/**
 * Some Twitter credentials are only able to read polls but not post polls to the desired account.
 * Other Twitter credentials are only able to write polls to the desired account but cannot read
 * the results of polls. Twitter is a curious thing. This enum is used for finding the correct
 * environment variables for the two sets of credentials needed for a Twitter poll game.
 */
public enum TwitterAccountRole {
    READING("reading"), WRITING("writing");

    private final String roleName;

    TwitterAccountRole(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
