package sondow.snake.conf;

import java.time.ZonedDateTime;

public class Time {

    public ZonedDateTime nowZonedDateTime() {
        return ZonedDateTime.now();
    }

    public void pauseBriefly() {
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
