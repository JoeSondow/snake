package sondow.snake.conf;

import java.util.ArrayList;
import java.util.List;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class BotConfigFactory {

    private Environment environment;

    public BotConfigFactory(Environment environment) {
        this.environment = environment;
    }

    public BotConfigFactory() {
        this(new Environment());
    }

    public BotConfig configure() {
        Configuration readingTwitterConf = configureTwitterRole(TwitterAccountRole.READING);
        Configuration writingTwitterConf = configureTwitterRole(TwitterAccountRole.WRITING);
        Integer turnMinutes = environment.getInt("turn_length_minutes", 20);
        if (turnMinutes == null) {
            turnMinutes = 20;
        } else if (turnMinutes < 5) {
            throw new RuntimeException("Twitter requires a minimum of 5 minutes for a poll but " +
                    "the turn_length_minutes environment variable is set to " + turnMinutes);
        }
        String airtableAccessToken = environment.require("cred_airtable_personal_access_token");
        String airtableBaseId = environment.require("cred_airtable_base");

        List<Configuration> publicityConfigs = new ArrayList<>();
        String publicityAccounts = environment.get("publicity_accounts");
        if (publicityAccounts != null) {
            String[] accounts = publicityAccounts.split(",");
            for (String account : accounts) {
                Configuration twitterConfig = configureTwitter(account, TrimUser.DISABLED);
                publicityConfigs.add(twitterConfig);
            }
        }

        return new BotConfig(readingTwitterConf, writingTwitterConf, turnMinutes,
                airtableAccessToken, airtableBaseId, publicityConfigs);
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, this custom code gets the configuration from
     * Lambda-friendly environment variables. Also, because of quirks of Twitter's poll API we often
     * need two sets of credentials, one for reading and one for writing.
     *
     * @param role the role of the account credentials we want, either reading or writing
     * @return configuration containing Twitter authentication strings and other variables
     */
    private Configuration configureTwitterRole(TwitterAccountRole role) {

        String roleName = role.getRoleName();

        return configureTwitter(roleName, TrimUser.ENABLED);
    }

    private Configuration configureTwitter(String roleName, TrimUser trimUser) {
        ConfigurationBuilder configBuilder = new ConfigurationBuilder();

        String credentialsCsv = environment.require("cred_twitter_" + roleName);

        String[] tokens = credentialsCsv.split(",");
        String screenName = tokens[0];
        String consumerKey = tokens[1];
        String consumerSecret = tokens[2];
        String accessToken = tokens[3];
        String accessTokenSecret = tokens[4];

        configBuilder.setUser(screenName);
        configBuilder.setOAuthConsumerKey(consumerKey);
        configBuilder.setOAuthConsumerSecret(consumerSecret);
        configBuilder.setOAuthAccessToken(accessToken);
        configBuilder.setOAuthAccessTokenSecret(accessTokenSecret);

        boolean trimDecision = trimUser.getValue();
        return configBuilder.setTrimUserEnabled(trimDecision).setTweetModeExtended(true).build();
    }
}
