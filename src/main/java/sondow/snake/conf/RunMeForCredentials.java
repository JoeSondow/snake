package sondow.snake.conf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * Note: as of mid-2019 this no longer helps acquire credentials for posting twitter polls nor
 * reading twitter poll results. Getting poll-capable credentials is now significantly more
 * difficult, and this class does not help. This class is still useful as a reference for how to
 * get credentials for ordinary twitter apps that do not post polls nor read poll results.
 * <p>
 * Run the main method of this class on your desktop computer one time in order to acquire
 * credentials for a twitter account to interact with the twitter API using the twitter app
 * specified by environment variables.
 * <p>
 * Copy the twitter app's consumer key and consumer secret credentials into environment variables.
 * <p>
 * Before running this, make sure the twitter4j.properties is currently empty, and that your current
 * default browser is already logged into the Twitter account you want to use.
 */
public class RunMeForCredentials {

    public static void main(String[] args) {
        try {
            Twitter twitter = new TwitterFactory().getInstance();

            // Use credentials that are able to post twitter polls and read poll results.
            Environment environment = new Environment();
            String consumerKey = environment.get("twitter4j_oauth_consumerKey");
            String consumerSecret = environment.get("twitter4j_oauth_consumerSecret");
            twitter.setOAuthConsumer(consumerKey, consumerSecret);

            try {
                // get request token.
                // this will throw IllegalStateException if access token is already available
                RequestToken requestToken = twitter.getOAuthRequestToken();
                System.out.println("Got request token.");
                System.out.println("Request token: " + requestToken.getToken());
                System.out.println("Request token secret: " + requestToken.getTokenSecret());
                AccessToken accessToken = null;

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                while (null == accessToken) {
                    System.out.println("Open the following URL and grant access to your account:");
                    System.out.println(requestToken.getAuthorizationURL());
                    System.out.print("Enter the PIN(if available) and hit enter after you granted" +
                            " access.[PIN]:");
                    String pin = br.readLine();
                    try {
                        if (pin.length() > 0) {
                            accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                        } else {
                            accessToken = twitter.getOAuthAccessToken(requestToken);
                        }
                    } catch (TwitterException te) {
                        if (401 == te.getStatusCode()) {
                            System.out.println("Unable to get the access token.");
                        } else {
                            te.printStackTrace();
                        }
                    }
                }
                System.out.println("Got access token.");
                System.out.println("Access token: " + accessToken.getToken());
                System.out.println("Access token secret: " + accessToken.getTokenSecret());
            } catch (IllegalStateException ie) {
                // access token is already available, or consumer key/secret is not set.
                if (!twitter.getAuthorization().isEnabled()) {
                    System.out.println("OAuth consumer key/secret is not set.");
                    System.exit(-1);
                }
            }
            System.exit(0);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to get timeline: " + te.getMessage());
            System.exit(-1);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }
    }
}
