package sondow.snake.conf;

import java.util.List;
import twitter4j.conf.Configuration;

public class BotConfig {

    /**
     * The configuration for reading from Twitter including reading poll results.
     * <p>
     * Credentials that can read poll results are now harder to get access to, and can be reused
     * from other accounts. Any account can read poll results of any other account, but cannot post
     * polls to other accounts.
     */
    private Configuration readingTwitterConfig;

    /**
     * The configuration for writing Twitter including creating new polls.
     *
     * Credentials that can create new polls are hard to get access to but not impossible, and they
     * must be owned by the account where the poll will be posted. They don't necessarily have the
     * ability to read poll results, though.
     */
    private Configuration writingTwitterConfig;

    /**
     * The airtable.com account's personal access token from https://airtable.com/create/tokens
     */
    private final String airtableAccessToken;

    /**
     * Log into airtable.com and go to https://airtable.com/api to see a list of your bases. Click
     * the base you want to use for this application, and note the ID string in the URL. This is not
     * the name of the base.
     * <p>
     * Example: https://airtable.com/apppPQyZsRLTu0ZCM/api/docs -> base id is apppPQyZsRLTu0ZCM
     */
    private final String airtableBaseId;

    /**
     * The number of minutes between turns, as specified by the TURN_LENGTH_MINUTES environment
     * variable which takes precedence over the length specified in the database, or the default of
     * 20.
     */
    private Integer turnLengthMinutes;

    /**
     * Configurations for API access for the twitter accounts that are to be used for retweeting the
     * most interesting game polls.
     */
    private final List<Configuration> publicityConfigs;

    BotConfig(Configuration readingTwitterConfig, Configuration writingTwitterConfig,
            Integer turnLengthMinutes, String airtableAccessToken, String airtableBaseId,
            List<Configuration> publicityConfigs) {

        this.readingTwitterConfig = readingTwitterConfig;
        this.writingTwitterConfig = writingTwitterConfig;
        this.turnLengthMinutes = turnLengthMinutes;
        this.airtableAccessToken = airtableAccessToken;
        this.airtableBaseId = airtableBaseId;
        this.publicityConfigs = publicityConfigs;
    }

    public Configuration getReadingTwitterConfig() {
        return readingTwitterConfig;
    }

    public Configuration getWritingTwitterConfig() {
        return writingTwitterConfig;
    }

    public Integer getTurnLengthMinutes() {
        return turnLengthMinutes;
    }

    public String getAirtableAccessToken() {
        return airtableAccessToken;
    }

    public String getAirtableBaseId() {
        return airtableBaseId;
    }

    public List<Configuration> getPublicityConfigs() {
        return publicityConfigs;
    }
}
