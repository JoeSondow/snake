package sondow.snake.game;

import com.google.common.collect.ImmutableList;
import java.time.ZonedDateTime;
import java.util.List;
import sondow.snake.io.EmojiSet;

public class Playthrough {

    private long id;

    private long firstTweetId;

    private ZonedDateTime startDate;

    private List<EmojiSet> themes;

    public Playthrough(long id, long firstTweetId, ZonedDateTime startDate, List<EmojiSet> themes) {

        this.id = id;
        this.firstTweetId = firstTweetId;

        // Deliberately lose minutes and seconds precision, to match database format
        this.startDate = startDate.withMinute(0).withSecond(0);

        this.themes = ImmutableList.copyOf(themes);
    }

    public long getId() {
        return id;
    }

    public long getFirstTweetId() {
        return firstTweetId;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public List<EmojiSet> getThemes() {
        return themes;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof Playthrough)) { return false; }

        Playthrough that = (Playthrough) o;

        if (getId() != that.getId()) { return false; }
        if (getFirstTweetId() != that.getFirstTweetId()) { return false; }
        if (getStartDate() != null ? !getStartDate().equals(that.getStartDate()) :
                that.getStartDate() != null) { return false; }
        return getThemes() != null ? getThemes().equals(that.getThemes()) :
                that.getThemes() == null;
    }

    @Override public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (int) (getFirstTweetId() ^ (getFirstTweetId() >>> 32));
        result = 31 * result + (getStartDate() != null ? getStartDate().hashCode() : 0);
        result = 31 * result + (getThemes() != null ? getThemes().hashCode() : 0);
        return result;
    }

    @Override public String toString() {
        return "Playthrough{" +
                "id=" + id +
                ", firstTweetId=" + firstTweetId +
                ", startDate=" + startDate +
                ", themes=" + themes +
                '}';
    }
}
