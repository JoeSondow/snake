package sondow.snake.game;

public class GameSummary {

    private final String gameRendering;
    private final int atomicMovesSinceEating;

    public GameSummary(String gameRendering, int atomicMovesSinceEating) {
        this.gameRendering = gameRendering;
        this.atomicMovesSinceEating = atomicMovesSinceEating;
    }

    public String getGameRendering() {
        return gameRendering;
    }

    public int getAtomicMovesSinceEating() {
        return atomicMovesSinceEating;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        GameSummary that = (GameSummary) o;

        if (getAtomicMovesSinceEating() != that.getAtomicMovesSinceEating()) { return false; }
        return getGameRendering() != null ? getGameRendering().equals(that.getGameRendering()) :
                that.getGameRendering() == null;
    }

    @Override public int hashCode() {
        int result = getGameRendering() != null ? getGameRendering().hashCode() : 0;
        result = 31 * result + getAtomicMovesSinceEating();
        return result;
    }

    @Override public String toString() {
        return "GameSummary{" +
                "gameRendering='" + gameRendering + '\'' +
                ", atomicMovesSinceEating=" + atomicMovesSinceEating +
                '}';
    }
}
