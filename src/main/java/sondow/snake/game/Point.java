package sondow.snake.game;

public class Point {

    private final int row;
    private final int col;

    public Point(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override public String toString() {
        return "Point{" + "row=" + row + ", col=" + col + '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Point point = (Point) o;

        if (getRow() != point.getRow()) { return false; }
        return getCol() == point.getCol();
    }

    @Override public int hashCode() {
        int result = getRow();
        result = 31 * result + getCol();
        return result;
    }
}
