package sondow.snake.game;

import com.google.common.collect.ImmutableList;
import java.time.ZonedDateTime;
import java.util.List;
import sondow.snake.io.EmojiSet;

public class GameRecord implements Comparable<GameRecord> {

    private final long id;

    private final List<EmojiSet> themes;

    private final int score;

    private final long firstTweetId;

    /** Hour precision, UTC */
    private final ZonedDateTime start;

    private final long conclusionTweetId;

    /** Hour precision, UTC */
    private final ZonedDateTime end;

    public GameRecord(long id, List<EmojiSet> themes, int score, long firstTweetId,
            ZonedDateTime start, long conclusionTweetId, ZonedDateTime end) {
        this.id = id;
        this.themes = ImmutableList.copyOf(themes);
        this.score = score;
        this.firstTweetId = firstTweetId;

        // Deliberately lose minutes and seconds precision, to match database format
        this.start = start.withMinute(0).withSecond(0);

        this.conclusionTweetId = conclusionTweetId;

        // Deliberately lose minutes and seconds precision, to match database format
        this.end = end.withMinute(0).withSecond(0);
    }

    public long getId() {
        return id;
    }

    public List<EmojiSet> getThemes() {
        return themes;
    }

    public int getScore() {
        return score;
    }

    public long getFirstTweetId() {
        return firstTweetId;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public long getConclusionTweetId() {
        return conclusionTweetId;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof GameRecord)) { return false; }

        GameRecord record = (GameRecord) o;

        if (getId() != record.getId()) { return false; }
        if (getScore() != record.getScore()) { return false; }
        if (getFirstTweetId() != record.getFirstTweetId()) { return false; }
        if (getConclusionTweetId() != record.getConclusionTweetId()) { return false; }
        if (getThemes() != null ? !getThemes().equals(record.getThemes()) :
                record.getThemes() != null) { return false; }
        if (getStart() != null ? !getStart().equals(record.getStart()) :
                record.getStart() != null) {
            return false;
        }
        return getEnd() != null ? getEnd().equals(record.getEnd()) : record.getEnd() == null;
    }

    @Override public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getThemes() != null ? getThemes().hashCode() : 0);
        result = 31 * result + getScore();
        result = 31 * result + (int) (getFirstTweetId() ^ (getFirstTweetId() >>> 32));
        result = 31 * result + (getStart() != null ? getStart().hashCode() : 0);
        result = 31 * result + (int) (getConclusionTweetId() ^ (getConclusionTweetId() >>> 32));
        result = 31 * result + (getEnd() != null ? getEnd().hashCode() : 0);
        return result;
    }

    @Override public String toString() {
        return "GameRecord{" +
                "id=" + id +
                ", themes=" + themes +
                ", score=" + score +
                ", firstTweetId=" + firstTweetId +
                ", start=" + start +
                ", conclusionTweetId=" + conclusionTweetId +
                ", end=" + end +
                '}';
    }

    /**
     * Highest scores first, but for a tie, the earliest game to achieve that score goes first.
     *
     * @param o the other game record to compare
     * @return -1 if this one comes first,
     */
    @Override public int compareTo(GameRecord o) {
        int result;
        if (this.getScore() > o.getScore()) {
            result = -1;
        } else if (this.getScore() < o.getScore()) {
            result = 1;
        } else {
            result = Long.compare(this.getId(), o.getId());
        }
        return result;
    }
}
