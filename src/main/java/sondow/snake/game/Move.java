package sondow.snake.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Move {
    UP("⬆️ Up", "up"),
    LEFT("⬅️ Left", "lf"),
    RIGHT("➡️ Right", "ri"),
    DOWN("⬇️ Down", "dn");

    private static Map<String, Move> EMOJI_AND_TITLE_CASE_TO_MOVE = new HashMap<>();
    private static Map<String, Move> ABBREVIATION_TO_MOVE = new HashMap<>();
    private static Map<String, Move> LOWERCASE_LABEL_TO_MOVE = new HashMap<>();

    public static final List<Move> UP_LEFT_RIGHT_DOWN = Arrays.asList(UP, LEFT, RIGHT, DOWN);

    static {
        for (Move move : Move.values()) {
            ABBREVIATION_TO_MOVE.put(move.getAbbreviation(), move);
            EMOJI_AND_TITLE_CASE_TO_MOVE.put(move.getEmojiAndTitleCase(), move);
            LOWERCASE_LABEL_TO_MOVE.put(move.getLowercaseLabel(), move);
        }
    }

    private final String emojiAndTitleCase;
    private final String abbreviation;
    private final String lowercaseLabel;

    Move(String emoji, String abbreviation) {
        this.emojiAndTitleCase = emoji;
        this.abbreviation = abbreviation;
        this.lowercaseLabel = Move.lowercaseLettersOnly(emojiAndTitleCase);
    }

    /**
     * Chooses the matching Move for a specified label. Case-insensitive. All letters must match but
     * all non-letter characters will be disregarded. This allows for Twitter's occasional changes
     * to how it outputs emoji characters in poll choices to some Android and Java clients.
     * <p>
     * This makes the text-matching algorithm more robust against future unexpected Twitter changes
     * about which emoji characters they output to various clients, like when they stopped revealing
     * boxed arrow characters in poll choice labels to some Android and Java clients on Sep 13,
     * 2022, which broke some of my poll bots.
     * <p>
     * Only letters get compared. Numerals, emojis, whitespace, punctuation are all ignored.
     *
     * @param label the string to look up
     * @return the matching Move
     */
    public static Move fromLabel(String label) {
        String lowercaseLetters = lowercaseLettersOnly(label);
        return LOWERCASE_LABEL_TO_MOVE.get(lowercaseLetters);
    }

    public static String lowercaseLettersOnly(String input) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isLetter(c)) {
                char lower = Character.toLowerCase(c);
                builder.append(lower);
            }
        }
        return builder.toString();
    }

    /**
     * Tries to find a matching Move for a given emoji and title case string.
     *
     * @param emojiAndTitleCase the string to look up
     * @return the matching Move
     */
    public static Move fromEmojiAndTitleCase(String emojiAndTitleCase) {
        return EMOJI_AND_TITLE_CASE_TO_MOVE.get(emojiAndTitleCase);
    }

    /**
     * Tries to find a matching Move for a given abbreviation string, probably from database.
     *
     * @param abbreviation the abbreviation to look up
     * @return the matching Move
     */
    public static Move fromAbbreviation(String abbreviation) {
        return ABBREVIATION_TO_MOVE.get(abbreviation);
    }

    /**
     * Poll choice strings for moves show an emojiAndTitleCase, followed by a space, followed by
     * the title case version of the move name.
     *
     * @param moves the moves to convert
     * @return the list of string representations of the moves, with emojis included
     */
    public static List<String> toPollChoices(List<Move> moves) {
        List<String> choices = new ArrayList<>();
        for (Move move : moves) {
            choices.add(move.getEmojiAndTitleCase());
        }
        return choices;
    }

    String getEmojiAndTitleCase() {
        return emojiAndTitleCase;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    String getLowercaseLabel() {
        return lowercaseLabel;
    }
}
