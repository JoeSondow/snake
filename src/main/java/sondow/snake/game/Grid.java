package sondow.snake.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Data structure for holding a grid of strings, mostly for emoji and whitespace.
 *
 * @author @JoeSondow
 */
public class Grid {

    private String[][] table;

    /**
     * Creates a grid of cells based on the specified dimensions, with a starting value in every
     * cell.
     *
     * @param rows the number of rows the grid should have
     * @param cols the number of columns the grid should have
     * @param init the initial value to put in every cell
     */
    public Grid(int rows, int cols, String init) {
        assert rows > 0;
        assert cols > 0;
        table = new String[rows][cols];
        for (int r = 0; r < table.length; r++) {
            String[] row = table[r];
            for (int c = 0; c < row.length; c++) {
                table[r][c] = init;
            }
        }
    }

    public List<String> getRowsAsStrings() {

        List<String> rowStrings = new ArrayList<>();
        for (String[] row : table) {
            StringBuilder builder = new StringBuilder();
            for (String cell : row) {
                builder.append(cell);
            }
            rowStrings.add(builder.toString());
        }
        return rowStrings;
    }

    /**
     * Puts a string in the grid at specific coordinates.
     *
     * @param row   the zero-indexed row number
     * @param col   the zero-indexed column number
     * @param value the string to put in the grid
     */
    public void put(int row, int col, String value) {

        table[row][col] = value;
    }
}
