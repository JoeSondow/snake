package sondow.snake.game;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import sondow.snake.conf.BotConfig;
import sondow.snake.conf.BotConfigFactory;
import sondow.snake.conf.Environment;
import sondow.snake.conf.Logger;
import sondow.snake.conf.Time;
import sondow.snake.io.Abridger;
import sondow.snake.io.AirtableDatabase;
import sondow.snake.io.Converter;
import sondow.snake.io.Database;
import sondow.snake.io.EmojiSet;
import sondow.snake.io.Outcome;
import sondow.snake.io.Referee;
import sondow.snake.io.TwitterPollMaker;
import twitter4j.Card;
import twitter4j.Status;
import twitter4j.StatusWithCard;
import twitter4j.conf.Configuration;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Executes a game turn from start to finish.
 */
public class Player {

    private final static Logger log = Logger.getLogger(Player.class);

    private BotConfig botConfig;
    private Database database;
    private Random random;
    private TwitterPollMaker pollMaker;
    private String twitterHandle;
    private Time time;
    private EmojiSet emojiSet;

    /**
     * Constructor for unit tests.
     */
    public Player(Random random, Time time, Environment environment, Database database,
            TwitterPollMaker pollMaker, EmojiSet emojiSet) {
        init(environment, random, time, emojiSet);
        this.database = database;
        this.pollMaker = pollMaker;
    }

    /**
     * Constructor for production.
     */
    public Player() {
        Random random = new Random();
        init(new Environment(), random, new Time(), EmojiSet.pickOne(random));
        this.database = new AirtableDatabase(botConfig);
        Configuration readingTwitterConfig = botConfig.getReadingTwitterConfig();
        Configuration writingTwitterConfig = botConfig.getWritingTwitterConfig();
        this.pollMaker = new TwitterPollMaker(readingTwitterConfig, writingTwitterConfig);
    }

    private void init(Environment environment, Random random, Time time, EmojiSet emojiSet) {
        this.botConfig = new BotConfigFactory(environment).configure();
        this.twitterHandle = botConfig.getWritingTwitterConfig().getUser();
        this.random = random;
        this.time = time;
        this.emojiSet = emojiSet;
    }

    public Outcome play() {
        String stateJson = database.readGameState();
        Converter converter = new Converter(random);
        Game game;
        String previousGameString = null;
        boolean isNewGame = false;
        if (stateJson == null) {
            isNewGame = true;
            game = new Game(emojiSet, random);
            game.spawnSnake();
            game.spawnTarget();
        } else {
            game = converter.stateStringToGame(stateJson);
            emojiSet = game.getEmojiSet(); // Unnecessary but just in case
            previousGameString = game.toString();
        }
        Outcome outcome = readPreviousPollAndPostNewTweets(game, false);
        outcome.setPreviousGameString(previousGameString);
        outcome = retryIfNeeded(game, outcome);
        List<Status> tweets = outcome.getTweets();
        if (tweets != null && tweets.size() >= 1) {
            Status lastTweet = tweets.get(tweets.size() - 1);
            game.setTweetId(lastTweet.getId());
            game.setThreadLength(game.getThreadLength() + tweets.size());
            if (game.isGameOver()) {
                updateDatabaseWithGameOverAndTweetHighScores(game, lastTweet);
            } else {
                String stateString = converter.gameToStateString(game);
                if (isNewGame) {
                    Status firstTweet = tweets.get(0);
                    updateDatabaseWithNewGame(firstTweet, stateString);
                } else {
                    database.writeGameState(stateString);
                }
            }
        }
        return outcome;
    }

    private Outcome retryIfNeeded(Game game, Outcome outcome) {
        int maxRetries = 6;
        for (int i = 1; i <= maxRetries && outcome.getRetryDelaySeconds() >= 1; i++) {
            log.info("retry " + i + ", outcome.getRetryDelaySeconds(): " + outcome
                    .getRetryDelaySeconds());
            waitForPollEnd(outcome.getRetryDelaySeconds());
            // Sometimes Twitter takes way too long to process finishing a poll, and I don't want
            // to wait indefinitely for that opaque and unreliable process to finish.
            boolean giveUpAndDoItAnyway = (i >= maxRetries);
            outcome = readPreviousPollAndPostNewTweets(game, giveUpAndDoItAnyway);
        }
        return outcome;
    }

    private void updateDatabaseWithNewGame(Status firstTweet, String stateString) {
        Converter converter = new Converter(random);
        long playId = getPlaythroughId(converter);
        long firstTweetId = firstTweet.getId();
        Instant startInstant = firstTweet.getCreatedAt().toInstant();
        ZonedDateTime startDate = ZonedDateTime.ofInstant(startInstant, ZoneOffset.UTC);
        List<EmojiSet> themes = Collections.singletonList(emojiSet);
        Playthrough playthru = new Playthrough(playId, firstTweetId, startDate, themes);
        String playthroughString = converter.playthroughToString(playthru);
        database.writeStateAndPlaythrough(stateString, playthroughString);
    }

    private long getPlaythroughId(Converter converter) {
        // Find previous game id in database or start at 1 if none found
        String playthruDb = database.readPlaythrough();
        long playId;
        if (playthruDb == null) {
            playId = 1L;
        } else {
            Playthrough prevPlaythru = converter.stringToPlaythrough(playthruDb);
            playId = prevPlaythru.getId() + 1;
        }
        return playId;
    }

    /**
     * Fetch high scores list, add this game record, store updated high scores list in database,
     * sort and tweet high scores list, appended to Game Over tweet.
     *
     * @param game      the game to get data from
     * @param lastTweet the Game Over tweet
     */
    private void updateDatabaseWithGameOverAndTweetHighScores(Game game, Status lastTweet) {

        PlaythroughAndScores playthroughAndScores = database.readPlaythroughAndScores();
        String playthroughString = playthroughAndScores.getPlaythrough();
        String oldScoresString = playthroughAndScores.getScores();
        Converter converter = new Converter(random);
        Playthrough playthrough = converter.stringToPlaythrough(playthroughString);

        GameRecord record = constructGameRecord(game, lastTweet, playthrough);
        List<GameRecord> gameRecords = converter.stringToGameRecords(oldScoresString);
        Scorekeeper scorekeeper = new Scorekeeper(40);
        List<GameRecord> newRecords = scorekeeper.addRecordAndSortAndTruncate(gameRecords, record);
        String highScoresTweetContent = buildHighScoresMessage(record, newRecords);

        String newScores = converter.gameRecordsToString(newRecords);
        database.deleteStateAndUpdateScores(newScores);

        log.info("High scores tweet content:\n" + highScoresTweetContent);

        pollMaker.tweet(highScoresTweetContent, lastTweet.getId(), null);
    }

    private String buildHighScoresMessage(GameRecord newest, List<GameRecord> highScores) {
        StringBuilder builder = new StringBuilder();
        builder.append("High Scores:\n\n");
        for (int i = 0; i < highScores.size() && i < 10; i++) {
            if (i >= 1) {
                builder.append("\n");
            }
            GameRecord iteration = highScores.get(i);
            if (newest.getId() == iteration.getId()) {
                builder.append("* ");
            }

            builder.append(i + 1).append(". ").append(iteration.getScore());
            builder.append(" #").append(iteration.getId()).append(" ");
            iteration.getThemes().forEach(e -> builder.append(e.getBody()));
            long hours = ChronoUnit.HOURS.between(iteration.getStart(), iteration.getEnd());
            builder.append(" ").append(hours).append("h");

            if (newest.getId() == iteration.getId()) {
                builder.append(" *");
            }
        }
        return builder.toString();
    }

    private GameRecord constructGameRecord(Game game, Status lastTweet, Playthrough playthrough) {
        long id = playthrough.getId();
        List<EmojiSet> themes = playthrough.getThemes();
        int score = game.getScore();
        long firstTweetId = playthrough.getFirstTweetId();
        ZonedDateTime start = playthrough.getStartDate();
        long endTweetId = lastTweet.getId();
        Instant endInstant = lastTweet.getCreatedAt().toInstant();
        ZonedDateTime end = ZonedDateTime.ofInstant(endInstant, ZoneOffset.UTC);

        return new GameRecord(id, themes, score, firstTweetId, start, endTweetId, end);
    }

    private Outcome readPreviousPollAndPostNewTweets(Game game, boolean giveUpAndDoItAnyway) {
        Outcome outcome = new Outcome();
        Integer turnLengthMinutes = botConfig.getTurnLengthMinutes();
        Long tweetId = game.getTweetId();
        StatusWithCard previous = pollMaker.readPreviousTweet(tweetId);
        if (previous == null || shouldAssessResultsNow(giveUpAndDoItAnyway, outcome, previous)) {
            List<Status> tweets = postTweets(game, turnLengthMinutes, pollMaker, previous);
            outcome.setTweets(tweets);
        }
        return outcome;
    }

    private boolean shouldAssessResultsNow(boolean giveUpAndDoItAnyway, Outcome
            outcome, StatusWithCard previousTweet) {

        Card poll = previousTweet.getCard();
        boolean countsAreFinal = poll.isCountsAreFinal();
        log.info("are poll counts final? " + countsAreFinal);
        boolean assessResultsNow;
        if (giveUpAndDoItAnyway || countsAreFinal) {
            assessResultsNow = true;
        } else {
            Referee referee = new Referee();
            if (referee.hasSuperMajority(poll)) {
                assessResultsNow = true;
            } else {
                long tweetId = previousTweet.getId();
                long pollTimeSecondsRemaining = calculatePollSecondsRemaining(poll);
                if (pollTimeSecondsRemaining > 60) {
                    throw new RuntimeException("Close race ends much later. secondsRemaining=" +
                            pollTimeSecondsRemaining + " tweetId=" + tweetId);
                }
                outcome.setRetryDelaySeconds(pollTimeSecondsRemaining + 5);
                assessResultsNow = false;
            }
        }
        return assessResultsNow;
    }

    private List<Status> postTweets(Game game, Integer turnLengthMinutes,
            TwitterPollMaker pollMaker, StatusWithCard previousGameTweet) {
        Referee referee = new Referee();
        Long idToReplyTo = null;
        boolean timeToBreakThread = (game.getThreadLength() >= 50);
        List<GameSummary> gameSummaries = new ArrayList<>();
        if (previousGameTweet == null) {
            // New game. Start from scratch.
            String text = game.toString();
            gameSummaries.add(new GameSummary(text, game.getSnake().getAtomicMovesSinceEating()));
        } else {
            final long previousGameTweetId = previousGameTweet.getId();
            idToReplyTo = previousGameTweetId; // If continuing thread
            Move move = referee.determineElectedMove(previousGameTweet);
            if (move != null) {
                try {
                    gameSummaries.addAll(game.doCompositeMove(move));
                } catch (GameOverException e) {
                    log.info("Game over with score " + game.getScore());
                }
            }

            if (timeToBreakThread) {
                // Start a new thread.

                // Three new tweets.

                // 1. newThreadIntroTweet: In reply to null, the start of the new thread should
                // be a tweet, possibly with a short message explaining that we're continuing a
                // game from another thread, and a link back to the last game tweet of the old
                // thread.
                // 2. newGameTweet: In reply to tweet 1, newThreadIntroTweet, make the
                // newGameTweet.
                // 3. oldThreadOutroTweet: In reply to the last game tweet of the old thread, a
                // tweet containing a link to tweet 2, newGameTweet.
                String previousGameTweetUrl = buildTweetUrl(previousGameTweetId);
                String newThreadIntroText = "Continuing game from thread… " + previousGameTweetUrl;
                Status newThreadIntroTweet = pollMaker.tweet(newThreadIntroText, null, null);
                idToReplyTo = newThreadIntroTweet.getId();
                time.pauseBriefly(); // Avoid tweeting too rapidly
            }
        }

        List<Status> newTweets = new ArrayList<>();

        // Assigned null for compilation. In practice, the for loop should always have at least
        // one iteration.
        Status newTweet = null;
        Abridger abridger = new Abridger();
        List<GameSummary> abridgedGameSummaries = abridger.abridge(gameSummaries);
        assert abridgedGameSummaries.size() >= 1;

        // Iterate through the gameRenderings. Assume that the last rendering will be either a game
        // poll tweet or a game over screen.
        for (int i = 0; i < abridgedGameSummaries.size(); i++) {
            if (i > 0) {
                time.pauseBriefly(); // Avoid tweeting too rapidly
            }
            GameSummary gameSummary = abridgedGameSummaries.get(i);
            String text = gameSummary.getGameRendering();
            boolean hasPoll = true;
            // If it's not the last one in the list then expect it's an intermediary with no poll
            if (i <= abridgedGameSummaries.size() - 2) {
                hasPoll = false;
            } else if (game.isGameOver()) {
                hasPoll = false;
            }
            if (hasPoll) {
                List<String> choices = Move.toPollChoices(game.listLegalMoves());
                newTweet = pollMaker.postPoll(turnLengthMinutes, text, choices, idToReplyTo);
            } else {
                // This is an intermediary progress tweet or a game over tweet
                newTweet = pollMaker.tweet(text, idToReplyTo, null);
            }
            idToReplyTo = newTweet.getId();
            newTweets.add(newTweet);
        }

        if (previousGameTweet != null && timeToBreakThread) {

            String newGameTweetUrl = buildTweetUrl(newTweet.getId());
            time.pauseBriefly(); // Avoid tweeting too rapidly
            pollMaker.tweet("Game continues in new thread… " + newGameTweetUrl,
                    previousGameTweet.getId(), null);

            game.setThreadLength(0);
        }

        return newTweets;
    }

    private String buildTweetUrl(long id) {
        return "https://twitter.com/" + twitterHandle + "/status/" + id;
    }

    private long calculatePollSecondsRemaining(Card poll) {
        Instant endDatetimeUtc = poll.getEndDatetimeUtc();
        Instant now = Instant.now();
        long pollTimeSecondsRemaining = Math.max(0, now.until(endDatetimeUtc, SECONDS));
        log.info("now = " + now + ", endDatetimeUtc = " + endDatetimeUtc +
                ", pollTimeSecondsRemaining = " + pollTimeSecondsRemaining);
        return pollTimeSecondsRemaining;
    }

    private void waitForPollEnd(long secondsToWait) {
        try {
            log.info("Waiting " + secondsToWait + " seconds.");
            Thread.sleep(secondsToWait * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) {
        Player player = new Player();
        player.play();
    }
}
