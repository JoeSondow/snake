package sondow.snake.game;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Keeps track of high scores.
 */
public class Scorekeeper {

    private int maxRecordCount;

    public Scorekeeper(int maxRecordCount) {
        this.maxRecordCount = maxRecordCount;
    }

    public List<GameRecord> addRecordAndSortAndTruncate(List<GameRecord> gameRecords,
            GameRecord record) {

        TreeSet<GameRecord> sorted = new TreeSet<>(gameRecords);
        sorted.add(record);
        List<GameRecord> sortedList = new ArrayList<>(sorted);
        // Limit the size of the list to store in the database. Maximum 40 items. Discard the lowest
        // scoring playthroughs.
        int countOfRecordsToKeep = Math.min(maxRecordCount, sortedList.size());
        return sortedList.subList(0, countOfRecordsToKeep);
    }
}
