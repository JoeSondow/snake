package sondow.snake.game;

public class PlaythroughAndScores {

    private String playthrough;
    private String scores;

    public PlaythroughAndScores(String playthrough, String scores) {
        this.playthrough = playthrough;
        this.scores = scores;
    }

    public String getPlaythrough() {
        return playthrough;
    }

    public String getScores() {
        return scores;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof PlaythroughAndScores)) { return false; }

        PlaythroughAndScores that = (PlaythroughAndScores) o;

        if (getPlaythrough() != null ? !getPlaythrough().equals(that.getPlaythrough()) :
                that.getPlaythrough() != null) { return false; }
        return getScores() != null ? getScores().equals(that.getScores()) :
                that.getScores() == null;
    }

    @Override public int hashCode() {
        int result = getPlaythrough() != null ? getPlaythrough().hashCode() : 0;
        result = 31 * result + (getScores() != null ? getScores().hashCode() : 0);
        return result;
    }

    @Override public String toString() {
        return "PlaythroughAndScores{" +
                "playthrough='" + playthrough + '\'' +
                ", scores='" + scores + '\'' +
                '}';
    }
}
