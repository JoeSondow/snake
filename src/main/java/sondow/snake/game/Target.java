package sondow.snake.game;

/**
 * Usually represented by an apple emoji.
 */
public class Target {

    private final int row;
    private final int col;

    public Target(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public boolean isAdjacentTo(Point otherPoint) {
        int rowDist = Math.abs(otherPoint.getRow() - this.getRow());
        int colDist = Math.abs(otherPoint.getCol() - this.getCol());
        int adjacent = Game.POINTS_TO_ADJACENT_INTERSECTION;
        return (rowDist == 0 && colDist == adjacent) || (rowDist == adjacent && colDist == 0);
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Target target = (Target) o;

        if (getRow() != target.getRow()) { return false; }
        return getCol() == target.getCol();
    }

    @Override public int hashCode() {
        int result = getRow();
        result = 31 * result + getCol();
        return result;
    }
}
