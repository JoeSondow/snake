package sondow.snake.io;

import java.util.function.BiConsumer;
import java.util.function.Function;

public enum DatabaseField {
    STATE("state", AirtableRecord::getState, AirtableRecord::setState),
    PLAYTHROUGH("playthrough", AirtableRecord::getPlaythrough, AirtableRecord::setPlaythrough),
    SCORES("scores", AirtableRecord::getScores, AirtableRecord::setScores);

    public final String fieldName;
    private final Function<AirtableRecord, String> fieldGetter;
    private BiConsumer<AirtableRecord, String> fieldSetter;

    DatabaseField(String fieldName, Function<AirtableRecord, String> fieldGetter,
            BiConsumer<AirtableRecord, String> fieldSetter) {
        this.fieldName = fieldName;
        this.fieldGetter = fieldGetter;
        this.fieldSetter = fieldSetter;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Function<AirtableRecord, String> getFieldGetter() {
        return fieldGetter;
    }

    public BiConsumer<AirtableRecord, String> getFieldSetter() {
        return fieldSetter;
    }
}
