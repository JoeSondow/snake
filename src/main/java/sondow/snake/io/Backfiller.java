package sondow.snake.io;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import sondow.snake.conf.Logger;
import sondow.snake.game.GameRecord;

public class Backfiller {

    private final static Logger log = Logger.getLogger(Backfiller.class);

    public static void main(String[] args) {
        backfillScoresFromArchive();
    }

    private static void backfillScoresFromArchive() {
        String dirPath = "C:\\Users\\joesondow\\Downloads\\" +
                "EmojiSnakeGame-tweet-archive-2019-06-22\\data\\js\\tweets";
        File dir = new File(dirPath);
        File[] jsFiles = dir.listFiles((dir1, name) -> name.endsWith(".js"));
        List<GameRecord> starts = new ArrayList<>();
        List<GameRecord> ends = new ArrayList<>();
        String regionalIndicatorG = "\uD83C\uDDEC";
        assert jsFiles != null;
        for (File file : jsFiles) {
            String path = file.getPath();
            String contents;
            try {
                contents = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String json = contents.substring(contents.indexOf("\n") + 1);
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = (JsonArray) parser.parse(json);

            for (JsonElement element : jsonArray) {
                JsonObject jsonObject = (JsonObject) element;
                JsonElement textElement = jsonObject.get("text");
                String text = textElement.getAsString();
                JsonElement inReplyToStatusIdStrElem = jsonObject.get("in_reply_to_status_id_str");
                if (inReplyToStatusIdStrElem == null && text.contains("Score 0")) {
                    // Start of a thread. Maybe start of a game, maybe not.

                    String createdAtStr = jsonObject.get("created_at").getAsString();
                    ZonedDateTime startDate = ZonedDateTime.parse(createdAtStr, DateTimeFormatter
                            .ofPattern("yyyy-MM-dd HH:mm:ss Z"));
                    long id = jsonObject.get("id").getAsLong();
                    Predicate<EmojiSet> themeInText = e -> text.contains(e.getBody());
                    Collector<EmojiSet, ?, List<EmojiSet>> toList = Collectors.toList();
                    List<EmojiSet> sets = EmojiSet.common();
                    List<EmojiSet> matches = sets.stream().filter(themeInText).collect(toList);
                    assert matches.size() == 1;
                    GameRecord start = new GameRecord(0, matches, 0, id, startDate, -1, startDate);
                    starts.add(start);
                } else if (text.contains(regionalIndicatorG)) {
                    // End of a game marked by regional indicator "G"
                    String createdAtStr = jsonObject.get("created_at").getAsString();
                    ZonedDateTime endDate = ZonedDateTime.parse(createdAtStr, DateTimeFormatter
                            .ofPattern("yyyy-MM-dd HH:mm:ss Z"));
                    long id = jsonObject.get("id").getAsLong();
                    String scoreLine = text.substring(0, text.indexOf("\n"));
                    int score = Integer.parseInt(scoreLine.substring(scoreLine.indexOf(" ") + 1));
                    GameRecord end = new GameRecord(0, new ArrayList<>(), score, -1, endDate, id
                            , endDate);
                    ends.add(end);
                }
            }
        }
        starts.sort(Comparator.comparingLong(GameRecord::getFirstTweetId));
        ends.sort(Comparator.comparingLong(GameRecord::getConclusionTweetId));
        log.info(starts.size());
        log.info(ends.size());
        //        for (GameRecord record : starts) {
        //            log.info(record);
        //        }
        List<GameRecord> fullRecords = new ArrayList<>();
        for (int i = 0; i < ends.size(); i++) {
            GameRecord end = ends.get(i);

            // Find the corresponding start.
            long conclusionTweetId = end.getConclusionTweetId();
            // Get all the starting tweet ids that are earlier (lesser) than the conclusion tweet id
            Predicate<GameRecord> isEarlier = (r -> r.getFirstTweetId() < conclusionTweetId);
            Comparator<GameRecord> compareIds = Comparator.comparing(GameRecord::getFirstTweetId);
            Supplier<RuntimeException> excep = RuntimeException::new;
            GameRecord start = starts.stream().filter(isEarlier).max(compareIds).orElseThrow(excep);
            GameRecord startEndRecord = new GameRecord(
                    i + 1, start.getThemes(), end.getScore(), start
                    .getFirstTweetId(), start.getStart(), end.getConclusionTweetId(), end.getEnd());
            fullRecords.add(startEndRecord);
        }
        fullRecords.sort(Comparator.comparingLong(GameRecord::getFirstTweetId));
        log.info(fullRecords.size());
        for (GameRecord record : fullRecords) {
            log.info(record);
        }
        Converter converter = new Converter(new Random());
        String scoresString = converter.gameRecordsToString(fullRecords);
        System.out.println(scoresString);
    }
}
