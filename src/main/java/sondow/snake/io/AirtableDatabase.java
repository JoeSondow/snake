package sondow.snake.io;

import com.sybit.airtable.Airtable;
import com.sybit.airtable.Base;
import com.sybit.airtable.Query;
import com.sybit.airtable.Table;
import com.sybit.airtable.exception.AirtableException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import sondow.snake.conf.BotConfig;
import sondow.snake.conf.BotConfigFactory;
import sondow.snake.conf.Logger;
import sondow.snake.game.PlaythroughAndScores;

public class AirtableDatabase implements Database {

    private final static Logger log = Logger.getLogger(AirtableDatabase.class);

    private final BotConfig config;
    private Airtable airtable;
    private Table<AirtableRecord> table;
    private HashMap<String, AirtableRecord> cache;

    AirtableDatabase(Airtable airtable, BotConfig config) {
        this(config);
        this.airtable = airtable;
    }

    public AirtableDatabase(BotConfig config) {
        cache = new HashMap<>();
        this.config = config;
    }

    public static void main(String[] args) {
        AirtableDatabase database = new AirtableDatabase(new BotConfigFactory().configure());
        database.deleteStateAndUpdateScores("newscores");
        // String state = database.readGameState();
        // System.out.println(state);
        // database.writeGameState("boom");
    }

    private String makeCacheMapKey(DatabaseField[] fields) {
        return Arrays.toString(fields);
    }

    @Override public String readGameState() {
        return readField(DatabaseField.STATE);
    }

    @Override public String readPlaythrough() {
        return readField(DatabaseField.PLAYTHROUGH);
    }

    @Override public String readScores() {
        return readField(DatabaseField.SCORES);
    }

    private String readField(DatabaseField field) {
        String fieldValue;
        AirtableRecord record = getAirtableRecord(field);
        fieldValue = nullifyEmptyString(field.getFieldGetter().apply(record));
        return fieldValue;
    }

    private String nullifyEmptyString(String fieldValue) {
        if (fieldValue != null && fieldValue.length() <= 0) {
            fieldValue = null;
        }
        return fieldValue;
    }

    private AirtableRecord getAirtableRecord(DatabaseField... fields) {

        // Convert the fields array into an object that is suitable as a hashmap key
        String fieldsKey = makeCacheMapKey(fields);

        // Use a cache map of each set of fields to each resulting record, to reduce database reads
        // for the same information, while avoiding a bug of finding no data in a field only
        // because it's being read from the cache but that field wasn't requested the first time.
        AirtableRecord cachedRecord = cache.get(fieldsKey);
        if (cachedRecord == null) {
            Table<AirtableRecord> table = getTable();
            String screenName = config.getWritingTwitterConfig().getUser();
            Query query = new AirtableQuery(screenName, Arrays.asList(fields));
            List results;
            try {
                results = table.select(query);
                if (results.size() <= 0) {
                    createRecord(screenName);
                    results = table.select(query);
                }
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }
            assert results.size() == 1;
            AirtableRecord fetchedRecord = (AirtableRecord) results.get(0);
            for (DatabaseField field : fields) {
                String value = field.getFieldGetter().apply(fetchedRecord);
                log.info("Reading " + field.getFieldName() + " from database: " + value);
            }
            cache.put(fieldsKey, fetchedRecord);
        }
        return cache.get(fieldsKey);
    }

    @SuppressWarnings("TryWithIdenticalCatches") // Because AWS Lambda only accepts Java 8
    private void createRecord(String screenName) {
        Table<AirtableRecord> table = getTable();
        AirtableRecord record = new AirtableRecord();
        record.setName(screenName);
        try {
            table.create(record);
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private Table<AirtableRecord> getTable() {
        if (table == null) {
            Base base;
            try {
                if (airtable == null) {
                    airtable = new Airtable().configure(config.getAirtableAccessToken());
                }
                base = airtable.base(config.getAirtableBaseId());
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }
            table = base.table("SNAKE", AirtableRecord.class);
        }
        return table;
    }

    @Override public void writeGameState(String state) {
        writeField(DatabaseField.STATE, state);
    }

    @Override public void writePlaythrough(String playthrough) {
        writeField(DatabaseField.PLAYTHROUGH, playthrough);
    }

    @Override public void writeScores(String scores) {
        writeField(DatabaseField.SCORES, scores);
    }

    private void writeField(DatabaseField field, String value) {
        Map<DatabaseField, String> map = new LinkedHashMap<>();
        map.put(field, value);
        writeFields(map);
    }

    @SuppressWarnings("TryWithIdenticalCatches") // Because AWS Lambda only accepts Java 8
    private void writeFields(Map<DatabaseField, String> fieldsToValues) {
        Table<AirtableRecord> table = getTable();
        DatabaseField[] databaseFields = fieldsToValues.keySet().toArray(new DatabaseField[0]);
        AirtableRecord record = getAirtableRecord(databaseFields);
        for (DatabaseField field : databaseFields) {
            String value = fieldsToValues.get(field);
            field.getFieldSetter().accept(record, value);
            log.info("Writing " + field.getFieldName() + " to database: " + value);
        }
        try {
            table.update(record);
            // Update the local cache map too
            String cacheMapKey = makeCacheMapKey(databaseFields);
            cache.put(cacheMapKey, record);
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void writeStateAndPlaythrough(String stateString, String playthroughString) {
        Map<DatabaseField, String> map = new LinkedHashMap<>();
        map.put(DatabaseField.STATE, stateString);
        map.put(DatabaseField.PLAYTHROUGH, playthroughString);
        writeFields(map);
    }

    @Override public PlaythroughAndScores readPlaythroughAndScores() {
        AirtableRecord record = getAirtableRecord(DatabaseField.PLAYTHROUGH, DatabaseField.SCORES);
        String playthrough = nullifyEmptyString(record.getPlaythrough());
        String scores = nullifyEmptyString(record.getScores());
        return new PlaythroughAndScores(playthrough, scores);
    }

    @Override public void deleteStateAndUpdateScores(String newScores) {
        Map<DatabaseField, String> map = new LinkedHashMap<>();
        map.put(DatabaseField.STATE, ""); // Empty string should erase value in database
        map.put(DatabaseField.SCORES, newScores);
        writeFields(map);
    }
}
