package sondow.snake.io;

import com.google.common.math.LongMath;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sondow.snake.game.Game;
import sondow.snake.game.GameRecord;
import sondow.snake.game.Playthrough;
import sondow.snake.game.Point;
import sondow.snake.game.Snake;
import sondow.snake.game.Target;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * Converts stuff into other stuff, mainly text to snake game and back.
 *
 * @author @JoeSondow
 */
public class Converter {

    private Random random;

    public Converter(Random random) {
        this.random = random;
    }

    public Game stateStringToGame(String json) {
        try {
            return parse(json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private Game parse(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        String themeName = obj.getString("thm");
        EmojiSet emojiSet = EmojiSet.valueOf(themeName);
        Game game = new Game(emojiSet, random);
        int score = obj.getInt("sc");
        long tweetId = obj.getLong("id");
        int threadLength = obj.getInt("thr");
        JSONArray targetCoordinates = obj.getJSONArray("tg");
        int targetRow = targetCoordinates.getInt(0);
        int targetRCol = targetCoordinates.getInt(1);
        Target target = new Target(targetRow, targetRCol);
        JSONArray snakeData = obj.getJSONArray("sn");
        int headRow = snakeData.getInt(0);
        int headCol = snakeData.getInt(1);
        Snake snake = new Snake();
        snake.addKeyPoint(headRow, headCol);
        String snakeTraversal = snakeData.getString(2);
        char[] snakeTraversalChars = snakeTraversal.toCharArray();
        int atomicMovesSinceEating = 0;
        if (snakeData.length() >= 4) {
            atomicMovesSinceEating = snakeData.getInt(3);
        }

        int lastRow = headRow;
        int lastCol = headCol;

        // Divide up the snake traversal characters into separate letter-number segments.
        // This makes indexing easier to understand.
        List<String> traversalSegments = new ArrayList<>();
        StringBuilder segmentBuilder = null; // Must start null so empty segment not added to list
        for (int i = 0; i < snakeTraversalChars.length; i++) {
            char ch = snakeTraversalChars[i];
            if (Character.isUpperCase(ch)) {
                if (segmentBuilder != null) {
                    traversalSegments.add(segmentBuilder.toString());
                }
                segmentBuilder = new StringBuilder();
            }
            segmentBuilder.append(ch);
        }
        // Add the last segment to the list.
        traversalSegments.add(segmentBuilder.toString());

        for (String traversalSegment : traversalSegments) {
            char direction = traversalSegment.charAt(0);
            String digits = traversalSegment.substring(1);
            int length = Integer.parseInt(digits);
            // Handle U D L R
            if (direction == 'U') {
                lastRow -= length;
            } else if (direction == 'D') {
                lastRow += length;
            } else if (direction == 'L') {
                lastCol -= length;
            } else if (direction == 'R') {
                lastCol += length;
            }
            snake.addKeyPoint(lastRow, lastCol);
        }
        snake.setAtomicMovesSinceEating(atomicMovesSinceEating);

        game.setSnake(snake).setScore(score).setTarget(target).setTweetId(tweetId);

        game.setThreadLength(threadLength);
        return game;
    }

    public String gameToStateString(Game game) {

        Target target = game.getTarget();
        int[] targetDataArray = new int[2];
        targetDataArray[0] = target.getRow();
        targetDataArray[1] = target.getCol();
        Snake snake = game.getSnake();
        Object[] snakeDataArray = buildEncodedSnakeObjectArray(snake);
        Map<String, Object> gameMap = new HashMap<>();
        gameMap.put("sn", snakeDataArray);
        gameMap.put("tg", targetDataArray);
        gameMap.put("thm", game.getEmojiSet().name());
        gameMap.put("sc", game.getScore());
        gameMap.put("thr", game.getThreadLength());
        gameMap.put("id", game.getTweetId());

        return new JSONObject(gameMap).toString();
    }

    private Object[] buildEncodedSnakeObjectArray(Snake snake) {
        Object[] snakeDataArray = new Object[4];
        List<Point> keyPoints = snake.getKeyPoints();
        Point head = keyPoints.get(0);
        int row = head.getRow();
        int col = head.getCol();
        snakeDataArray[0] = row;
        snakeDataArray[1] = col;
        StringBuilder traversalStringBuilder = new StringBuilder();
        for (int i = 1; i < keyPoints.size(); i++) {
            Point point = keyPoints.get(i);
            int curRow = point.getRow();
            int curCol = point.getCol();
            char direction;
            int length;
            if (curRow < row) {
                direction = 'U';
                length = row - curRow;
            } else if (curRow > row) {
                direction = 'D';
                length = curRow - row;
            } else if (curCol < col) {
                direction = 'L';
                length = col - curCol;
            } else if (curCol > col) {
                direction = 'R';
                length = curCol - col;
            } else {
                throw new RuntimeException("Invalid point list " + keyPoints);
            }
            traversalStringBuilder.append(direction).append(length);
            row = curRow;
            col = curCol;
        }
        snakeDataArray[2] = traversalStringBuilder.toString();
        snakeDataArray[3] = snake.getAtomicMovesSinceEating();
        return snakeDataArray;
    }

    private static Pattern PLAYTHROUGH_PATTERN = Pattern.compile("" +
            "([0-9]+)" + // Game ID
            "-" + // Separator
            "([0-9]+)" + // First tweet ID
            "-" + // Separator
            "([0-9]+)" + // Start date year, month, day, hour like 2019013104
            "-" + // Separator
            "([a-z]+)" // Emoji sets
    );

    public Playthrough stringToPlaythrough(String dbString) {
        Playthrough playthrough;
        Matcher matcher = PLAYTHROUGH_PATTERN.matcher(dbString);
        if (matcher.matches()) {
            String idString = matcher.group(1);
            String firstTweetIdString = matcher.group(2);
            String startDateString = matcher.group(3);
            String emojiSetCodes = matcher.group(4);
            int id = Integer.parseInt(idString);
            long firstTweetId = Long.parseLong(firstTweetIdString);
            ZonedDateTime startDate = parseDate(startDateString);
            List<EmojiSet> emojiSets = getEmojiSetsFromCodeLetters(emojiSetCodes);
            playthrough = new Playthrough(id, firstTweetId, startDate, emojiSets);
        } else {
            throw new RuntimeException("Error parsing playthrough " + dbString);
        }

        return playthrough;
    }

    private ZonedDateTime parseCompressedDate(String compressedStartDate) {
        String dateString = "" + expandCompressedInteger(compressedStartDate);
        return parseDate(dateString);
    }

    private ZonedDateTime parseDate(String dateString) {
        return ZonedDateTime.parse(dateString, NUMERICAL_FORMATTER);
    }

    private List<EmojiSet> getEmojiSetsFromCodeLetters(String emojiSetCodes) {
        List<EmojiSet> emojiSets = new ArrayList<>();
        for (int i = 0; i < emojiSetCodes.length(); i++) {
            char code = emojiSetCodes.charAt(i);
            EmojiSet emojiSet = EmojiSet.fromCode("" + code);
            emojiSets.add(emojiSet);
        }
        return emojiSets;
    }

    public String playthroughToString(Playthrough playthrough) {
        long id = playthrough.getId();
        long firstTweetId = playthrough.getFirstTweetId();
        ZonedDateTime startDate = playthrough.getStartDate();
        String startDateString = startDate.format(NUMERICAL_FORMATTER);
        List<EmojiSet> emojiSets = playthrough.getThemes();
        String themes = makeThemeCodesString(emojiSets);
        return id + "-" + firstTweetId + "-" + startDateString + "-" + themes;
    }

    private static Pattern GAME_RECORD_PATTERN = Pattern.compile("" +
            "([0-9]+)" + // Game ID
            "([a-z]+)" + // Emoji sets
            "([0-9]+)" + // Score
            "-" + // Separator
            "([^-]+)" + // Compressed base 89 first tweet ID, non-hyphen characters
            "-" + // Separator
            "([^-]+)" + // Compressed base 89 start date year, month, day, hour like 2019013104
            "-" + // Separator
            "([^-]+)" + // Compressed base 89 conclusion tweet ID, non-hyphen characters
            "-" + // Separator
            "([^-]+)" // Compressed base 89 end date year, month, day, hour like 2019013104
    );

    private DateTimeFormatter NUMERICAL_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("yyyyMMddHH").toFormatter().withZone(ZoneOffset.UTC);

    public List<GameRecord> stringToGameRecords(String dbString) {
        List<GameRecord> records = new ArrayList<>();

        if (dbString != null && dbString.length() >= 1) {
            String[] sections = dbString.split(",");
            for (String section : sections) {
                Matcher matcher = GAME_RECORD_PATTERN.matcher(section);
                if (matcher.matches()) {
                    String idString = matcher.group(1);
                    String emojiSetCodes = matcher.group(2);
                    String scoreString = matcher.group(3);
                    String compressedFirstTweetId = matcher.group(4);
                    String compressedStartDate = matcher.group(5);
                    String compressedConclusionTweetId = matcher.group(6);
                    String compressedEndDate = matcher.group(7);

                    int id = Integer.parseInt(idString);
                    List<EmojiSet> emojiSets = getEmojiSetsFromCodeLetters(emojiSetCodes);
                    int score = Integer.parseInt(scoreString);
                    long firstTweetId = expandCompressedInteger(compressedFirstTweetId);
                    ZonedDateTime start = parseCompressedDate(compressedStartDate);
                    long conclusionTweetId = expandCompressedInteger(compressedConclusionTweetId);
                    ZonedDateTime end = parseCompressedDate(compressedEndDate);
                    GameRecord record = new GameRecord(id, emojiSets, score, firstTweetId, start,
                            conclusionTweetId, end);
                    records.add(record);
                } else {
                    throw new RuntimeException(
                            "Error parsing section " + section + " in high scores list: " +
                                    dbString);
                }
            }
        }
        return records;
    }

    public String gameRecordsToString(List<GameRecord> records) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < records.size(); i++) {
            if (i > 0) {
                builder.append(",");
            }
            GameRecord record = records.get(i);
            long id = record.getId();
            List<EmojiSet> emojiSets = record.getThemes();
            String themes = makeThemeCodesString(emojiSets);
            int score = record.getScore();
            String compressedFirstTweetId = compressInteger(record.getFirstTweetId());
            String startFormatted = record.getStart().format(NUMERICAL_FORMATTER);
            String compressedStart = compressInteger(Long.parseLong(startFormatted));
            String compressedConclusionTweetId = compressInteger(record.getConclusionTweetId());
            String endFormatted = record.getEnd().format(NUMERICAL_FORMATTER);
            String compressedEnd = compressInteger(Long.parseLong(endFormatted));
            builder.append(id).append(themes).append(score).append("-").
                    append(compressedFirstTweetId).append("-").
                    append(compressedStart).append("-").
                    append(compressedConclusionTweetId).append("-").
                    append(compressedEnd);
        }
        return builder.toString();
    }

    private String makeThemeCodesString(List<EmojiSet> emojiSets) {
        StringBuilder themesStringBuilder = new StringBuilder();
        emojiSets.forEach(t -> themesStringBuilder.append(t.getCode()));
        return themesStringBuilder.toString();
    }

    // I could use extended ascii characters to increase the base radix and the compression factor
    // but then the database storage of the encoded string data has a risk of nasty surprises
    // because there is no global standard for extended ascii. So we'll just use ascii.
    // 94 visible ascii characters
    // 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`-=[]\;',./~!@#$%^&*()
    // _+{}|:"<>?
    // Reserve -|\," for separators, escaping, and string closing.
    // 89 remaining ascii characters
    // 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`=[];'./~!@#$%^&*()_+{}:<>?

    private static final String DIGITS = "0123456789abcdefghijklmnopqrstuvwxyz" +
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ`=[];'./~!@#$%^&*()_+{}:<>?";

    public String compressInteger(long i) {

        // Mostly similar to Long.toString(long i, int radix)
        int radix = DIGITS.length();
        char[] buf = new char[65];
        int charPos = 64;
        boolean negative = (i < 0);

        if (!negative) {
            i = -i;
        }

        while (i <= -radix) {
            buf[charPos--] = Converter.DIGITS.charAt((int) (-(i % radix)));
            i = i / radix;
        }
        buf[charPos] = Converter.DIGITS.charAt((int) (-i));

        if (negative) {
            buf[--charPos] = '-';
        }

        return new String(buf, charPos, (65 - charPos));
    }

    public long expandCompressedInteger(String compressed) {
        // Maybe borrow some ideas from Long.parseLong(int, int)

        int radix = DIGITS.length();
        long accumulation = 0;

        // Reverse in order to start with the least precision first, and to have the index of the
        // character match with the exponent to which we will raise the radix for this place.
        String reversed = new StringBuilder(compressed).reverse().toString();

        for (int i = 0; i < reversed.length(); i++) {
            char digit = reversed.charAt(i);

            // The value of the digit is its plain value multiplied by radix ^ i
            long value = DIGITS.indexOf(digit) * LongMath.checkedPow(radix, i);
            long newAccumulation = accumulation + value;

            //noinspection ConstantConditions because newAccumulation could overflow and be negative
            if (value < 0 || newAccumulation < accumulation || newAccumulation < 0) {
                throw new NumberFormatException("Overflow error reading compressed string '" +
                        compressed + "' on iteration " + i + " for compressed digit " + digit +
                        " holding decimal value " + value + " resulting in new accumulation " +
                        newAccumulation + " after previous accumulation " + accumulation);
            }
            accumulation = newAccumulation;
        }

        return accumulation;
    }
}
