package sondow.snake.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import static sondow.snake.io.EmojiSetType.COMMON;
import static sondow.snake.io.EmojiSetType.DEV;

public enum EmojiSet {

    TEST("s", "🔵", "🔺", "💚", "🔴", DEV),

    BLUE("u", "🔵", "🍎", "💚", "🔴", COMMON),
    BREAD("b", "🍞", "🥐", "🥞", "🥨", COMMON),
    DONUT("d", "🍩", "🍭", "🥧", "🥜", COMMON),
    FORTUNE("f", "🔮", "🥠", "✨", "🎱", COMMON),
    GIFT("g", "🎁", "🍰", "💝", "📦", COMMON),
    MOON("m", "🌕", "🧀", "☀", "🌒", COMMON),
    PEACH("p", "🍑", "🍒", "🍓", "🥔", COMMON),
    TREE("t", "🌳", "🍊", "🎄", "🍂", COMMON);

    final String code;
    final String wall = "⬛";
    final String body;
    final String target;
    final String excitedHead = "😛";
    final String satedHead = "😋";
    final String contentHead = "🙂";
    final String neutralHead = "😐";
    final String starvingHead = "😟";
    final String winnerHead = "😃";
    final String winnerBody;
    final String loserHead = "😞";
    final String loserBody;
    final String loserTarget = "💥";
    final EmojiSetType type;

    EmojiSet(String code, String body, String target, String winnerBody, String loserBody,
            EmojiSetType type) {
        this.code = code;
        this.type = type;
        this.body = body;
        this.target = target;
        this.winnerBody = winnerBody;
        this.loserBody = loserBody;
    }

    /**
     * The braille blank character seems to be the only character that twitter renders as whitespace
     * but does not trim off the start of a tweet and does not ignore when comparing two tweets to
     * check for duplicate text. This makes it ideal for appending to a tweet that would otherwise
     * be considered an illegal duplicate by Twitter's standards.
     */
    public static final String brailleBlank = "⠀";

    private static Map<Integer, String> numbersToClocks = new HashMap<>();

    static {
        numbersToClocks.put(0, "🚫");
        numbersToClocks.put(1, "🕐");
        numbersToClocks.put(2, "🕑");
        numbersToClocks.put(3, "🕒");
        numbersToClocks.put(4, "🕓");
        numbersToClocks.put(5, "🕔");
        numbersToClocks.put(6, "🕕");
        numbersToClocks.put(7, "🕖");
        numbersToClocks.put(8, "🕗");
        numbersToClocks.put(9, "🕘");
        numbersToClocks.put(10, "🕙");
        numbersToClocks.put(11, "🕚");
        numbersToClocks.put(12, "🕛");
    }

    public String getClock(int hour) {
        String emoji;
        if (hour >= 12) {
            emoji = numbersToClocks.get(12);
        } else if (hour <= 0) {
            emoji = numbersToClocks.get(0);
        } else {
            emoji = numbersToClocks.get(hour);
        }
        return emoji;
    }

    public String getBlank() {
        return type.getBlank();
    }

    public String getCode() {
        return code;
    }

    public String getWall() {
        return wall;
    }

    public String getBody() {
        return body;
    }

    public String getTarget() {
        return target;
    }

    public String getExcitedHead() {
        return excitedHead;
    }

    public String getSatedHead() {
        return satedHead;
    }

    public String getContentHead() {
        return contentHead;
    }

    public String getNeutralHead() {
        return neutralHead;
    }

    public String getStarvingHead() {
        return starvingHead;
    }

    public String getWinnerHead() {
        return winnerHead;
    }

    public String getWinnerBody() {
        return winnerBody;
    }

    public String getLoserHead() {
        return loserHead;
    }

    public String getLoserBody() {
        return loserBody;
    }

    public String getLoserTarget() {
        return loserTarget;
    }

    public static List<EmojiSet> common() {
        EmojiSet[] values = EmojiSet.values();
        List<EmojiSet> commonThemes = new ArrayList<>();
        for (int i = 0; i < values.length; i++) {
            EmojiSet emojiSet = values[i];
            if (emojiSet.type == COMMON) {
                commonThemes.add(emojiSet);
            }
        }
        return commonThemes;
    }

    public static EmojiSet pickOne(Random random) {
        List<EmojiSet> sets = common();
        return sets.get(random.nextInt(sets.size()));
    }

    public static EmojiSet fromCode(String code) {
        EmojiSet[] values = EmojiSet.values();
        List<EmojiSet> matches = new ArrayList<>();
        for (int i = 0; i < values.length; i++) {
            EmojiSet emojiSet = values[i];
            if (emojiSet.code.equals(code)) {
                matches.add(emojiSet);
            }
        }
        assert matches.size() == 1;
        return matches.get(0);
    }
}
