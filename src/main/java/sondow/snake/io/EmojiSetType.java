package sondow.snake.io;

public enum EmojiSetType {
    COMMON("▫️"),
    DEV("⭕");

    public String getBlank() {
        return blank;
    }

    private final String blank;

    EmojiSetType(String blank) {
        this.blank = blank;
    }
}