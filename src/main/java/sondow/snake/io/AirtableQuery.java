package sondow.snake.io;

import com.sybit.airtable.Query;
import com.sybit.airtable.Sort;
import java.util.List;
import java.util.Objects;

public class AirtableQuery implements Query {

    private String twitterScreenName;
    private List<DatabaseField> fieldNames;

    AirtableQuery(String twitterScreenName, List<DatabaseField> fieldNames) {
        this.twitterScreenName = twitterScreenName;
        this.fieldNames = fieldNames;
    }

    @Override public String[] getFields() {
        return fieldNames.stream().map(DatabaseField::getFieldName).toArray(String[]::new);
    }

    @Override public Integer getPageSize() {
        return null;
    }

    @Override public Integer getMaxRecords() {
        return null;
    }

    @Override public String getView() {
        return null;
    }

    @Override public List<Sort> getSort() {
        return null;
    }

    @Override public String filterByFormula() {
        return "name='" + twitterScreenName + "'";
    }

    @Override public String getOffset() {
        return null;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof AirtableQuery)) { return false; }

        AirtableQuery that = (AirtableQuery) o;

        if (!Objects.equals(twitterScreenName, that.twitterScreenName)) { return false; }
        return Objects.equals(fieldNames, that.fieldNames);
    }

    @Override public int hashCode() {
        int result = twitterScreenName != null ? twitterScreenName.hashCode() : 0;
        result = 31 * result + (fieldNames != null ? fieldNames.hashCode() : 0);
        return result;
    }

    @Override public String toString() {
        return "AirtableQuery{" +
                "twitterScreenName='" + twitterScreenName + '\'' +
                ", fieldNames=" + fieldNames +
                '}';
    }
}
