package sondow.snake.io;

import java.util.Date;

/**
 * An object-relational-mapping (ORM) Java bean to match a record in the Airtable database.
 */
public class AirtableRecord {
    private String id;
    private String name;
    private String state;
    private String playthrough;
    private String scores;
    private Date createdTime;

    /**
     * @return the unique, immutable identifier Airtable gives to the record
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name of the twitter handle where the game is running
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the compressed, text-encoded game state
     */
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPlaythrough() {
        return playthrough;
    }

    public void setPlaythrough(String playthrough) {
        this.playthrough = playthrough;
    }

    public String getScores() {
        return scores;
    }

    public void setScores(String scores) {
        this.scores = scores;
    }

    /**
     * @return when the record was created in the database (supplied by Airtable)
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof AirtableRecord)) { return false; }

        AirtableRecord that = (AirtableRecord) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) {
            return false;
        }
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) {
            return false;
        }
        if (getState() != null ? !getState().equals(that.getState()) : that.getState() != null) {
            return false;
        }
        if (getPlaythrough() != null ? !getPlaythrough().equals(that.getPlaythrough()) :
                that.getPlaythrough() != null) { return false; }
        if (getScores() != null ? !getScores().equals(that.getScores()) :
                that.getScores() != null) {
            return false;
        }
        return getCreatedTime() != null ? getCreatedTime().equals(that.getCreatedTime()) :
                that.getCreatedTime() == null;
    }

    @Override public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (getPlaythrough() != null ? getPlaythrough().hashCode() : 0);
        result = 31 * result + (getScores() != null ? getScores().hashCode() : 0);
        result = 31 * result + (getCreatedTime() != null ? getCreatedTime().hashCode() : 0);
        return result;
    }

    @Override public String toString() {
        return "AirtableRecord{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", playthrough='" + playthrough + '\'' +
                ", scores='" + scores + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }
}
