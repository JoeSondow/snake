package sondow.snake.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import sondow.snake.conf.Logger;
import sondow.snake.game.Move;
import twitter4j.Card;
import twitter4j.Choice;
import twitter4j.StatusWithCard;

/**
 * Judges poll results.
 */
public class Referee {

    private final static Logger log = Logger.getLogger(Referee.class);

    /**
     * If highest choice vote count is 70% of total or higher OR the winning choice is at least 40
     * percentage points higher than the second place winning choice, then there's no need to wait
     * until the end of the poll.
     *
     * @param card the twitter poll to analyze
     * @return true if poll has sufficient super majority to warrant calling election early
     */
    public boolean hasSuperMajority(Card card) {
        boolean isSuperMajority = false;
        Choice[] choices = card.getChoices();
        long totalVotes = 0;
        for (Choice choice : choices) {
            totalVotes += choice.getCount();
        }
        if (totalVotes >= 1) {
            List<Choice> choicesList = Arrays.asList(choices);
            choicesList.sort(Comparator.comparing(Choice::getCount).reversed());
            Choice winner = choicesList.get(0);
            Choice runnerUp = choicesList.get(1);
            long winnerPercentage = winner.getCount() * 100 / totalVotes;
            long runnerUpPercentage = runnerUp.getCount() * 100 / totalVotes;
            if (winnerPercentage >= 80 || winnerPercentage - runnerUpPercentage >= 50) {
                isSuperMajority = true;
            }
        }
        return isSuperMajority;
    }

    /**
     * @param previous the tweet to analyze
     * @return the Move chosen in the tweet, or null if no poll
     */
    public Move determineElectedMove(StatusWithCard previous) {
        Card card = previous.getCard();
        Move chosenMove = null;
        if (card != null) {
            Choice[] choices = card.getChoices();
            // Find which choice(s) got the most votes.
            long topCount = findTopCount(choices);
            List<Move> winners = findElectedChoices(choices, topCount);
            if (winners.size() == 1) {
                chosenMove = winners.get(0);
            } else {
                chosenMove = winners.get(new Random().nextInt(winners.size()));
            }
            log.info("Poll results for tweet " + previous.getId() + ": " + card);
        }
        return chosenMove;
    }

    private List<Move> findElectedChoices(Choice[] choices, long topCount) {
        List<Move> winners = new ArrayList<>();
        for (Choice choice : choices) {
            if (choice.getCount() >= topCount) {
                winners.add(Move.fromLabel(choice.getLabel()));
            }
        }
        return winners;
    }

    private long findTopCount(Choice[] choices) {
        long topCount = 0;
        for (Choice choice : choices) {
            if (choice.getCount() > topCount) {
                topCount = choice.getCount();
            }
        }
        return topCount;
    }
}
