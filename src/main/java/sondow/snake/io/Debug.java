package sondow.snake.io;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import twitter4j.HttpResponse;
import twitter4j.TwitterException;

public class Debug {

    public void logHttpResponse(HttpResponse response) {
        String responseAsString = null;
        try {
             responseAsString = response.asString();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        System.out.print("HttpResponse class: " + response.getClass().getName() + ", as json: " +
                responseAsString + ", body: " + response.toString() + ", headers: ");
        Map<String, List<String>> responseHeaderFields = response.getResponseHeaderFields();
        Set<String> keys = responseHeaderFields.keySet();
        Set<String> nonNullKeys = new TreeSet<>(); // Sort
        for (String key : keys) {
            if (key != null) {
                nonNullKeys.add(key);
            }
        }
        boolean isFirstKey = true;
        for (String key : nonNullKeys) {
            if (isFirstKey) {
                isFirstKey = false;
            } else {
                System.out.print(" | ");
            }
            if (key != null) {
                List<String> values = responseHeaderFields.get(key);
                System.out.print(key + " : ");
                boolean isFirstValue = true;
                for (String value : values) {
                    if (isFirstValue) {
                        isFirstValue = false;
                    } else {
                        System.out.print(" , ");
                    }
                    System.out.print(value);
                }
            }
        }
        System.out.println();
    }
}
