package sondow.snake.io;

import sondow.snake.game.PlaythroughAndScores;

public interface Database {

    String readGameState();

    String readPlaythrough();

    String readScores();

    void writeGameState(String state);

    void writePlaythrough(String playthrough);

    void writeScores(String scores);

    void writeStateAndPlaythrough(String stateString, String playthroughString);

    PlaythroughAndScores readPlaythroughAndScores();

    void deleteStateAndUpdateScores(String newScores);
}
