# Emoji Snake Game Questions and Answers

[[_TOC_]]

## General

[Emoji Snake Game](https://twitter.com/EmojiSnakeGame) is a twitter bot that uses twitter polls to 
make communal decisions for a game about a snake eating food and getting longer without getting
trapped by their own tail.

### Who made this?

[Joe Sondow](https://twitter.com/JoeSondow) wrote most of the code.

Some improvements to the visual design and game play are thanks to many clever people who chat with 
me when I [livestream my coding sessions on Twitch](https://twitch.tv/JoeSondow).

[Bradley Momberger](https://twitter.com/air_hadoken) and [Stefan Bohacek](https://twitter.com/fourtonfish) 
discovered and shared the method for performing read/write access to Twitter polls.

### Why did you make this?

I like making Twitter games. After the success of EmojiTetra, a lot of players requested a 
Snake implementation. If you'd like a longer answer, [here's a video of a talk I gave about my Twitter 
bots](https://twitter.com/JoeSondow/status/954424471510831104).

### Wait, there are other games like this?

Yes. Check out [@EmojiTetra](https://twitter.com/EmojiTetra).

### How can I help support continued development of this game?

If you have the means and the inclination, you can become a patron of my work on 
[Patreon](https://patreon.com/JoeSondow), or make a one-time payment via 
[PayPal](https://paypal.me/joesondow). Whatever you think it's worth. Every little bit helps 
motivate me to continue building twitter games. Tips are always appreciated and never required.

Another way to help is to retweet the game's poll tweets occasionally, or tweet about what you like
about the game, to help spread the word.

The easiest way of all to support this game is to play it.

### So now we have to pay for tweets?

No. The game is free. Some people like to support creators to encourage more creation. Some don't,
and that's okay. 

### My question isn't answered here. What do I do?

Live with the crushing disappointment, or tweet your question to me at 
[@JoeSondow](https://twitter.com/JoeSondow) and I'll do my best to address it.

## Rules

### Is there a maximum score?

Yes. 3460.

There are only 28 opportunities for a treat to spawn during a game. The maximum score each treat 
can yield is 120 points, plus 100 points for getting all 28 treats. So the maximum score is 
28 x 120 + 100 = 3460. This can only happen by chance, and the odds of a maximum score happening 
are in the neighborhood of 1/(3x10^29) each game. Each game takes about a day. Our sun runs out of 
hydrogen in less than 5 billion years, so our solar system will be long gone before this game gets 
the maximum score.

### Is the score based entirely on chance or is there an element of skill as well?

There is an element of skill. The chance factor comes from how conveniently placed each treat is 
when it spawns. The skill is in choosing when (and when not) to make a riskier snake shape for the 
sake of eating the treat in fewer moves for more points, without trapping the snake in its own body.
The safest strategies can also be the lowest scoring ones.

### How are ties broken? Like, if "Up" and "Right" get equal vote totals, what happens?

The system chooses one of the winning poll choices at random.

### What does the clock mean?

The clock emoji near the treat starts by pointing at 12:00. This signifies that if the snake eats
the treat right away, players earn 120 points. Each time the snake moves to a new intersection,
the clock ticks down one hour, signifying that the treat is worth 10 points less. For example, if
the snake eats the treat when the clock shows 5:00 then players earn 50 points.

### Why is the clock necessary?

The game initially launched without a clock, and the resulting fun level was underwhelming. Since
twitter poll games are about puzzle solving rather than dexterity, and the puzzle in any Snake game
choice is trivial, players were getting bored with the ease achieving a perfect victory almost
every time. Having the treats worth more at first and less later creates a tension between choosing
the safe path or the short path. This makes the choices more interesting and fun.

### How does the score board work?

Each treat eaten earns players 0 to 120 points, depending on what time shows on the clock emoji
near the treat. Each treat starts with a score value of 120 points. Each step the snake takes 
without eating the treat decreases the treat's score value by 10 points. When the value drops to
zero, it stays at zero. The snake must still eat the zero-points treat in order to spawn the next
treat.

If the snake fills up every intersection in the game grid, players earn an extra 100 points and the
game ends on a positive note.

### Does the clock tick down once for each poll or once for each intersection the snake head moves through?

Once for each intersection the snake head moves through. So, if the snake only has a long, narrow 
channel to move through with no choices along the way, then each automatic step the snake takes 
along that path causes the clock to tick down one unit, reducing the treat's point value.

### Why are there black squares on the white game grid?

The black squares are only there to enforce a separation between parallel line segments of the 
snake's body, so players can tell at a glance what shape the snake is in. Otherwise a long winding
snake would just be a jumble of emojis all touching in a big blob that doesn't resemble a snake so
players would not be able to discern how it will unravel.

### Does the snake's head always move to an intersection?

Yes. The intersections of the roadways of white squares are the main functional positions of the
game. The intersections are where the snake's head, tail end, and bend points can be. Treats only
spawn at intersections.

### Why are there never treats between black squares?

The playable points where the snake head can arrive are the intersections of the white roadways. The treats appear at those
intersections. Every intersection is eventually reachable by the snake head as long as the head 
chases its tail. If a treat could appear between any black squares, then if the snake were too long 
then it could be possible for the snake to be unable to reach that treat, which wouldn't be fun.

### Can the snake exit one side of the game grid to arrive at the opposite side?

No.

### Does the snake die if it hits a wall?

Because this is not a dexterity game, the snake never hits a wall nor its own tail unless trapped
on all sides.

### Can the snake pass through its own body?

No.

### Why are the blank spaces filled with white squares instead of white space or some other symbol?

Unfortunately, as of Mar 2019 there is no whitespace character that renders with the same width as 
emojis. The white square emojis are the best approximation I've found to having negative space in an 
emoji grid.

### What are the rules for the snake's facial expressions?

Emotions are too complex to predict algorithmically. Just kidding. It works like this: 

- If the game has been won, the snake looks elated.
- If the game has been lost, the snake looks disappointed.
- If the snake head is adjacent to a treat, the snake is excited and has their tongue hanging out.
- If the snake has just eaten a treat, they have a satisfied expression licking their lips. 
- If the snake has eaten a treat in the past 1-4 moves, they have a calm happy smile. 
- If they've eaten a treat in the past 5-8 moves, they have a neutral expression. 
- If they haven't eaten a treat in the past 8 moves, they have a worried, stressed out expression.

### When does the emoji theme change?

The program randomly chooses a theme at the start of each game.

### Is it possible for a single theme to occur in two sequential games?

Yes.

### How many emoji themes are there?

[Eight](https://gitlab.com/JoeSondow/snake/blob/1f97d6bf14b3d2c13bd46f8a3461e9aab5bfc322/src/main/java/sondow/snake/io/EmojiSet.java#L11), as of Mar 2019.

### Does the game restart after Game Over?

Yes.

### Why does the next game poll sometimes get tweeted before the previous poll has ended?

When the results of a poll show a supermajority for a single choice during the final few 
seconds, the bot assumes that it's safe to call that election and move on with the next move.

This is a workaround for how timers work in AWS Lambda and Twitter. Both systems have a time
granularity of a minute, not a second or millisecond, so I can only guarantee that poll start
and end times will occur sometime within a certain minute. 

This design decision is a tradeoff. If my higher priority were to guarantee that a previous 
poll ends before the next poll begins, I would have to make the polls end after 19 minutes 
instead of 20 minutes, leaving 1 minute out of every 20 as a time when there aren't any game 
polls to vote in. I think it's more fun to have at least one poll to vote in 100% of the 
time rather than 95% of the time, even if it means there are sometimes a few seconds where 
you can vote in two polls.

I prioritize maximizing fun more than guaranteeing correctness. The balance between those 
two concerns changes when incorrect results happen too often, because too much incorrectness 
reduces fun. Starting a poll slightly early after a supermajority result does not cause
frequent incorrectness.

### Can the snake move safely towards the very tip of it own tail?

Yes because the tail gets out of the way as the head arrives.

### What happens when the snake's length is maxed out?

The "Good Game" success emojis are displayed, with a happy snake head, an evolved snake body. 
Players earn an extra 100 points, and the game ends on a win.

### Is there a way to shrink the snake?

No. It only grows longer.

## Troubleshooting

### Why don't I see a poll sometimes?

If it's game over or showing an intermediary game state where the snake is moving along the only 
possible path then the tweet should not be expected to have a poll; a later tweet should. If the 
snake has multiple directions it could go in then there should be a poll; Twitter sometimes take a 
while before it renders the poll of a tweet. Give it a while, or maybe try refreshing the page or 
restarting the app.

### My phone doesn't show some of the characters correctly. What can be done?

Unfortunately it’s impossible to make a game like this that displays properly on all platforms. The
big three emoji environments that people use to read Twitter these days (as of 2019) are Android, 
iOS, and desktop/laptop computers. Blackberry and other environments account for less than 1% of
users, and they don't show many emojis correctly.

### Why does the losing end screen say "GAME OUER" instead of "GAME OVER"?

Samsung has its own emoji implementation, which 
[incorrectly renders the "Regional Indicator Symbol Letter V" as a U](https://emojipedia.org/regional-indicator-symbol-letter-v/).

### Why do close races occasionally result in a move that is not the final winner of a poll?

Players sometimes refer to this phenomenon as the Electoral College of Twitter polls. I think it’s 
more accurate to refer to it as voter suppression.

There are two different Twitter poll bugs that cause this result.

* Sometimes Twitter marks poll results “final” and then inexplicably changes the results later.
* Sometimes Twitter inexplicably waits a very long time before marking poll results final. When that 
happens, the program does its best to assess the results as they are shown after the poll 
has been over for a while, and then moves on with the next game state.

### Does the interval between polls get shorter as the score increases?

No.

### What changes are planned for the future?

The [Issue Boards](https://gitlab.com/JoeSondow/snake/boards) page shows all the issues waiting
to be addressed.

## Technical

### Why is this hosted on GitLab instead of GitHub?

GitLab is mostly a functional copy of GitHub, but GitLab provides free private repositories, and
I was put off of GitHub after reading [this story by one of GitHub's former employees](https://where.coraline.codes/blog/my-year-at-github/).
Also I like GitLab's [Issue Boards](https://gitlab.com/JoeSondow/snake/boards) a lot. 
GitHub doesn't have seem to have issue boards, as of 2019. 

### How long does a game last?

Usually about a day or two. It might get shorter if I add some features to speed it up.

### Why are the tweets shown as being posted by iOS?

That's the only way I've found so far to post twitter polls programmatically. Twitter developers
haven't added poll support to the official API.

### Where is this system deployed?

This bot is deployed on the [AWS Lambda](https://aws.amazon.com/lambda/) serverless code execution 
service, using the [Airtable](https://airtable.com/) database service, outputting to
[Twitter](https://twitter.com/EmojiSnakeGame).

### How was the bot made?

The project includes these technologies:
 
* [Java](http://www.oracle.com/technetwork/java/javase/overview/index.html)
* [Groovy](http://groovy-lang.org/) 
* [Gradle](https://maven.apache.org/)
* [Twitter4J](http://twitter4j.org/)
* [JUnit](https://junit.org)
* [Spock](http://spockframework.org/)
* [System Rules](https://stefanbirkner.github.io/system-rules/)
* [AWS SDK for Java](https://aws.amazon.com/sdk-for-java/)
* [Airtable.java](https://github.com/Sybit-Education/airtable.java)
* [Shadow plugin for Gradle](http://imperceptiblethoughts.com/shadow/)

### Why Java? Why not Ruby/Python/Groovy/Kotlin/Scala/JavaScript/Rust/Go/Assembly?

I like Java. I'm good at it. It has excellent tool support and a large, robust community. I have
more fun building stuff than learning more languages.

I used to code mostly in Groovy and JavaScript, but I now prefer the tool support for Java.

### How are the version numbers chosen?

I use [SemVer](http://semver.org/) for versioning. For the versions available, see 
the [tags on this repository](https://gitlab.com/JoeSondow/snake/tags).

### What is the license for this codebase?

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
